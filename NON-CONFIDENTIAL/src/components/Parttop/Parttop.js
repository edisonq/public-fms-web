import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
		Button,
		ButtonDropdown,
		Col,
		DropdownMenu,
		DropdownToggle,
		DropdownItem,
		Row,
} from 'reactstrap';

import s from './Parttop.scss';

class Parttop extends React.Component {
	static propTypes = {
		// dispatch: PropTypes.func.isRequired,
		// sidebarState: PropTypes.string.isRequired,
		// sidebarPosition: PropTypes.string.isRequired,
	};

	constructor(props) {
		super(props);
		this.toggleFive = this.toggleFive.bind(this);
		this.state = {
				dropdownOpenFive: false,
			};
	}

	toggleFive = () => {
		this.setState({
			dropdownOpenFive: !this.state.dropdownOpenFive,
		});
	};

	render() {
		return (
			    <Row>
				<Col lg={1} md={4} sm={6}>
					<NavLink to="/app/issueworkorders" activeClassName="" exact>
						<div className="icon">
							<span className="btn btn-inverse btn-lg btn-block">
							<i className="fa fa-sliders" />
							</span>
						</div>
					</NavLink>
					<div className="description">
						<strong>&nbsp;</strong> Adjust
					</div>
				</Col>
				<Col lg={1} md={4} sm={6}>
					<NavLink to="/app/fleet/repair" activeClassName="" exact>
						<div className="icon">
							<span className="btn btn-inverse btn-lg btn-block">
								<i className="fa fa-exchange" />
							</span>
						</div>
					</NavLink>
					<div className="description">
						<strong>&nbsp;</strong> Transfer
					</div>
				</Col>
				<Col lg={1} md={4} sm={6}>
					<NavLink to="/app/fleet/inspect" activeClassName="" exact>
						<div className="icon">
							<span className="btn btn-inverse btn-lg btn-block">
								<i className="glyphicon glyphicon-shopping-cart" />
							</span>
						</div>
					</NavLink>
					<div className="description">
						<strong>&nbsp;</strong> Create PO
					</div>
				</Col>
				<Col lg={1} md={4} sm={6}>
					<NavLink to="/app/fleet/fuel" activeClassName="" exact>
						<div className="icon">
							<span className="btn btn-inverse btn-lg btn-block">
								<i className="fa fa-history" />
							</span>
						</div>
					</NavLink>
					<div className="description">
						<strong>&nbsp;</strong> History
					</div>
				</Col>
				
				<Col lg={1} md={4} sm={6}>
					<div>
					<ButtonDropdown
							className="mr-xs"
							isOpen={this.state.dropdownOpenFive}
							toggle={this.toggleFive} 
							block
						>
						{/* <Button block size="lg" color="primary">New</Button> */}
						<NavLink to="/app/fleet/new" activeClassName="" exact>
							<div className="icon" color="primary">
								<span className="btn btn-primary btn-lg btn-block primary">
									New
								</span>
							</div>
						</NavLink>	
						<DropdownToggle caret color="primary" className="dropdown-toggle-split btn-lg" />
						<DropdownMenu>
							<DropdownItem>Record Accident</DropdownItem>
							<DropdownItem>Record Fluid Usage</DropdownItem>
							<DropdownItem>Meter</DropdownItem>
							<DropdownItem>Tire Assignment</DropdownItem>
							<DropdownItem divider />
							<DropdownItem>Last PM Setup</DropdownItem>
							<DropdownItem divider />
							<DropdownItem>Edit</DropdownItem>
							<DropdownItem>Delete</DropdownItem>
						</DropdownMenu>
					</ButtonDropdown>
					</div>
				</Col>
			</Row>
		);
	}
}

function mapStateToProps(store) {
//   return {
		// sidebarState: store.navigation.sidebarState,
		// sidebarPosition: store.navigation.sidebarPosition,
//   };
}

// export default withStyles(s)(Parttop);
export default withRouter(withStyles(s)(Parttop));