import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router';

// an example of react-router code-splitting
/* eslint-disable */
import loadUIIcons from 'bundle-loader?lazy!../../pages/ui-elements/icons';
import loadUIButtons from 'bundle-loader?lazy!../../pages/ui-elements/buttons';
import loadUIAccordion from 'bundle-loader?lazy!../../pages/ui-elements/accordion';
import loadUITabs from 'bundle-loader?lazy!../../pages/ui-elements/tabs';
import loadUINotifications from 'bundle-loader?lazy!../../pages/ui-elements/notifications';
import loadUIDialogs from 'bundle-loader?lazy!../../pages/ui-elements/dialogs';
import loadComponentsCalendar from 'bundle-loader?lazy!../../pages/components/calendar';
import loadComponentsMaps from 'bundle-loader?lazy!../../pages/components/maps';
import loadComponentsGallery from 'bundle-loader?lazy!../../pages/components/gallery';
import loadComponentsFileupload from 'bundle-loader?lazy!../../pages/components/fileupload';
import loadFormsAccount from 'bundle-loader?lazy!../../pages/forms/account';
import loadFormsArticle from 'bundle-loader?lazy!../../pages/forms/article';
import loadFormsElements from 'bundle-loader?lazy!../../pages/forms/elements';
import loadFormsValidation from 'bundle-loader?lazy!../../pages/forms/validation';
import loadFormsWizard from 'bundle-loader?lazy!../../pages/forms/wizard';
import loadStatisticsStats from 'bundle-loader?lazy!../../pages/statistics/stats';
import loadStatisticsCharts from 'bundle-loader?lazy!../../pages/statistics/charts';
import loadStatisticsRealtime from 'bundle-loader?lazy!../../pages/statistics/realtime';
import loadTablesStatic from 'bundle-loader?lazy!../../pages/tables/static';
import loadTablesDynamic from 'bundle-loader?lazy!../../pages/tables/dynamic';
import loadWidgetsBasic from 'bundle-loader?lazy!../../pages/widgets/basic';
import loadWidgetsLive from 'bundle-loader?lazy!../../pages/widgets/live';
import loadSpecialSearch from 'bundle-loader?lazy!../../pages/special/search';
import loadSpecialInvoice from 'bundle-loader?lazy!../../pages/special/invoice';
import loadSpecialInbox from 'bundle-loader?lazy!../../pages/special/inbox';

/* custom bundle loader */
import loadFleetNew from 'bundle-loader?lazy!../../pages/fleet/new';
import loadFleetAllList from 'bundle-loader?lazy!../../pages/fleet/all';
import loadFleetOverdueList from 'bundle-loader?lazy!../../pages/fleet/overdue';
import loadFleetSoondueList from 'bundle-loader?lazy!../../pages/fleet/soondue';
import loadFleetWOassigned from 'bundle-loader?lazy!../../pages/fleet/woassigned';
import loadFleetrepair from 'bundle-loader?lazy!../../pages/fleet/repair';
import loadFleetInspect from 'bundle-loader?lazy!../../pages/fleet/inspect';
import loadFleetFuel from 'bundle-loader?lazy!../../pages/fleet/fuel';
import loadFleetExpense from 'bundle-loader?lazy!../../pages/fleet/expense';
import loadFleetHistory from 'bundle-loader?lazy!../../pages/fleet/history';
import loadFleetTask from 'bundle-loader?lazy!../../pages/fleet/task';
import loadFleetParts from 'bundle-loader?lazy!../../pages/fleet/parts';
import loadInventoryParts from 'bundle-loader?lazy!../../pages/inventory/parts';
import loadInventoryFuel from 'bundle-loader?lazy!../../pages/inventory/fuel';
import loadInventoryTires from 'bundle-loader?lazy!../../pages/inventory/tires';
import loadInventoryEquipments from 'bundle-loader?lazy!../../pages/inventory/equipments';
import loadInventoryExpectedParts from 'bundle-loader?lazy!../../pages/inventory/expectedparts';
import loadIssues from 'bundle-loader?lazy!../../pages/issues';
import loadRenttoown from 'bundle-loader?lazy!../../pages/rentals/renttoown';
import loadRemittance from 'bundle-loader?lazy!../../pages/rentals/remittance';
import loadPurchasing from 'bundle-loader?lazy!../../pages/financials/purchasing';
import loadExpenses from 'bundle-loader?lazy!../../pages/financials/expenses';
import loadDeposits from 'bundle-loader?lazy!../../pages/financials/deposits';
import loadDebts from 'bundle-loader?lazy!../../pages/financials/debts';
import loadPayroll from 'bundle-loader?lazy!../../pages/financials/payroll';
import loadInvoices from 'bundle-loader?lazy!../../pages/financials/invoices';
import loadWorkorders from 'bundle-loader?lazy!../../pages/workorders';
import loadIssueworkorder from 'bundle-loader?lazy!../../pages/workorders/issueworkorder';
import loadServiceentries from 'bundle-loader?lazy!../../pages/serviceentries';
import loadLegals from 'bundle-loader?lazy!../../pages/legals';
import loadEmployees from 'bundle-loader?lazy!../../pages/people/employees';
import loadCustomers from 'bundle-loader?lazy!../../pages/people/customers';
import loadVendors from 'bundle-loader?lazy!../../pages/people/vendors';
import loadCalendar from 'bundle-loader?lazy!../../pages/calendar';
import loadInspections from 'bundle-loader?lazy!../../pages/inspections';
import loadGeoinspections from 'bundle-loader?lazy!../../pages/geofencing';


/* eslint-enable */

import s from './Layout.scss';
import Header from '../Header';
import Sidebar from '../Sidebar';
import Bundle from '../../core/Bundle';

// Dashboard & Package components are loaded directly as an example of server side rendering
import Dashboard from '../../pages/dashboard';
import Package from '../../pages/package';
import { vendor } from 'postcss';

const UIIconsBundle = Bundle.generateBundle(loadUIIcons);
const UIButtonsBundle = Bundle.generateBundle(loadUIButtons);
const UIAccordionBundle = Bundle.generateBundle(loadUIAccordion);
const UITabsBundle = Bundle.generateBundle(loadUITabs);
const UINotificationsBundle = Bundle.generateBundle(loadUINotifications);
const UIDialogsBundle = Bundle.generateBundle(loadUIDialogs);
const ComponentsCalendarBundle = Bundle.generateBundle(loadComponentsCalendar);
const ComponentsMapsBundle = Bundle.generateBundle(loadComponentsMaps);
const ComponentsGalleryBundle = Bundle.generateBundle(loadComponentsGallery);
const ComponentsFileuploadBundle = Bundle.generateBundle(loadComponentsFileupload);
const FormsAccountBundle = Bundle.generateBundle(loadFormsAccount);
const FormsArticleBundle = Bundle.generateBundle(loadFormsArticle);
const FormsElementsBundle = Bundle.generateBundle(loadFormsElements);
const FormsValidationBundle = Bundle.generateBundle(loadFormsValidation);
const FormsWizardBundle = Bundle.generateBundle(loadFormsWizard);
const StatisticsStatsBundle = Bundle.generateBundle(loadStatisticsStats);
const StatisticsChartsBundle = Bundle.generateBundle(loadStatisticsCharts);
const StatisticsRealtimeBundle = Bundle.generateBundle(loadStatisticsRealtime);
const TablesStaticBundle = Bundle.generateBundle(loadTablesStatic);
const TablesDynamicBundle = Bundle.generateBundle(loadTablesDynamic);
const WidgetsBasicBundle = Bundle.generateBundle(loadWidgetsBasic);
const WidgetsLiveBundle = Bundle.generateBundle(loadWidgetsLive);
const SpecialSearchBundle = Bundle.generateBundle(loadSpecialSearch);
const SpecialInvoiceBundle = Bundle.generateBundle(loadSpecialInvoice);
const SpecialInboxBundle = Bundle.generateBundle(loadSpecialInbox);

const FleetNew = Bundle.generateBundle(loadFleetNew);
const FleetAllList = Bundle.generateBundle(loadFleetAllList);
const FleetOverdueList = Bundle.generateBundle(loadFleetOverdueList);
const FleetSoondueList = Bundle.generateBundle(loadFleetSoondueList);
const FleetWOassigned = Bundle.generateBundle(loadFleetWOassigned);
const Fleetrepair = Bundle.generateBundle(loadFleetrepair);
const FleetInspect = Bundle.generateBundle(loadFleetInspect);
const FleetFuel = Bundle.generateBundle(loadFleetFuel);
const FleetExpense = Bundle.generateBundle(loadFleetExpense);
const FleetTask = Bundle.generateBundle(loadFleetTask);
const FleetParts = Bundle.generateBundle(loadFleetParts);
const FleetHistory = Bundle.generateBundle(loadFleetHistory);

const InventoryParts = Bundle.generateBundle(loadInventoryParts);
const InventoryFuel = Bundle.generateBundle(loadInventoryFuel);
const InventoryTires = Bundle.generateBundle(loadInventoryTires);
const InventoryEquipments = Bundle.generateBundle(loadInventoryEquipments);
const InventoryExpectedParts = Bundle.generateBundle(loadInventoryExpectedParts);
const Issues = Bundle.generateBundle(loadIssues);

const Renttoown = Bundle.generateBundle(loadRenttoown);
const Remittance = Bundle.generateBundle(loadRemittance);
const Purchasing = Bundle.generateBundle(loadPurchasing);
const Expenses = Bundle.generateBundle(loadExpenses);
const Deposits = Bundle.generateBundle(loadDeposits);
const Debts = Bundle.generateBundle(loadDebts);
const Payroll = Bundle.generateBundle(loadPayroll);
const Invoices = Bundle.generateBundle(loadInvoices);

const Workorderslist = Bundle.generateBundle(loadWorkorders);
const Issueworkorder = Bundle.generateBundle(loadIssueworkorder);

const Serviceentries = Bundle.generateBundle(loadServiceentries);
const Legals = Bundle.generateBundle(loadLegals);
const Employees = Bundle.generateBundle(loadEmployees);
const Customers = Bundle.generateBundle(loadCustomers);
const Vendors = Bundle.generateBundle(loadVendors);
const Calendar = Bundle.generateBundle(loadCalendar);
const Inspections = Bundle.generateBundle(loadInspections);
const Geofencing = Bundle.generateBundle(loadGeoinspections);


class Layout extends React.Component {

  static propTypes = {
    sidebarState: PropTypes.string.isRequired,
    sidebarPosition: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className={s.root}>
        <Header toggleSidebar={this.toggleSidebar} />
        <div className={`${s.wrap} ${this.props.sidebarState === 'hide' ? 'sidebar-hidden' : ''} ${this.props.sidebarPosition === 'right' ? 'sidebar-right' : ''}`}>
          <Sidebar />
          <main className={s.content}>
            <Switch>
              <Route path="/app" exact component={Dashboard} />
              <Route path="/app/package" exact component={Package} />
              <Route path="/app/ui/icons" exact component={UIIconsBundle} />
              <Route path="/app/ui/buttons" exact component={UIButtonsBundle} />
              <Route path="/app/ui/accordion" exact component={UIAccordionBundle} />
              <Route path="/app/ui/tabs" exact component={UITabsBundle} />
              <Route path="/app/ui/notifications" exact component={UINotificationsBundle} />
              <Route path="/app/ui/dialogs" exact component={UIDialogsBundle} />
              <Route path="/app/components/calendar" exact component={ComponentsCalendarBundle} />
              <Route path="/app/components/maps" exact component={ComponentsMapsBundle} />
              <Route path="/app/components/gallery" exact component={ComponentsGalleryBundle} />
              <Route path="/app/components/fileupload" exact component={ComponentsFileuploadBundle} />
              <Route path="/app/forms/account" exact component={FormsAccountBundle} />
              <Route path="/app/forms/article" exact component={FormsArticleBundle} />
              <Route path="/app/forms/elements" exact component={FormsElementsBundle} />
              <Route path="/app/forms/validation" exact component={FormsValidationBundle} />
              <Route path="/app/forms/wizard" exact component={FormsWizardBundle} />
              <Route path="/app/statistics/stats" exact component={StatisticsStatsBundle} />
              <Route path="/app/statistics/charts" exact component={StatisticsChartsBundle} />
              <Route path="/app/statistics/realtime" exact component={StatisticsRealtimeBundle} />
              <Route path="/app/tables/static" exact component={TablesStaticBundle} />
              <Route path="/app/tables/dynamic" exact component={TablesDynamicBundle} />
              <Route path="/app/widgets/basic" exact component={WidgetsBasicBundle} />
              <Route path="/app/widgets/live" exact component={WidgetsLiveBundle} />
              <Route path="/app/special/search" exact component={SpecialSearchBundle} />
              <Route path="/app/special/invoice" exact component={SpecialInvoiceBundle} />
              <Route path="/app/special/inbox" component={SpecialInboxBundle} />
              <Route path="/app/fleet/new" component={FleetNew} />
              <Route path="/app/fleet/all" component={FleetAllList} />
              <Route path="/app/fleet/overdue" component={FleetOverdueList} />
              <Route path="/app/fleet/soondue" component={FleetSoondueList} />
              <Route path="/app/fleet/woassigned" component={FleetWOassigned} />
              <Route path="/app/fleet/repair" component={Fleetrepair} />
              <Route path="/app/fleet/inspect" component={FleetInspect} />
              <Route path="/app/fleet/fuel" component={FleetFuel} />
              <Route path="/app/fleet/expense" component={FleetExpense} />
              <Route path="/app/fleet/task" component={FleetTask} />
              <Route path="/app/fleet/parts" component={FleetParts} />
              <Route path="/app/fleet/history" component={FleetHistory} />
              <Route path="/app/inventory/parts" component={InventoryParts} />
              <Route path="/app/inventory/fuel" component={InventoryFuel} />
              <Route path="/app/inventory/tires" component={InventoryTires} />
              <Route path="/app/inventory/equipments" component={InventoryEquipments} />
              <Route path="/app/inventory/expectedparts" component={InventoryExpectedParts} />
              <Route path="/app/issues" component={Issues} />
              <Route path="/app/rentals/renttoown" component={Renttoown} />
              <Route path="/app/rentals/remittance" component={Remittance} />
              <Route path="/app/financials/purchasing" component={Purchasing} />
              <Route path="/app/financials/expenses" component={Expenses} />
              <Route path="/app/financials/deposits" component={Deposits} />
              <Route path="/app/financials/Debts" component={Debts} />
              <Route path="/app/financials/payroll" component={Payroll} />
              <Route path="/app/financials/invoices" component={Invoices} />
              <Route path="/app/workorders" component={Workorderslist} />
              <Route path="/app/issueworkorders" component={Issueworkorder} />
              <Route path="/app/serviceentries" component={Serviceentries} />
              <Route path="/app/legals" component={Legals} />
              <Route path="/app/people/employees" component={Employees} />
              <Route path="/app/people/customers" component={Customers} />
              <Route path="/app/people/vendors" component={Vendors} />
              <Route path="/app/calendar" component={ComponentsCalendarBundle} />
              <Route path="/app/inspections" component={Inspections} />
              <Route path="/app/geofencing" component={Geofencing} /> 
                          
            </Switch>
            <footer className={s.footer}>
              MiFleet - Made by <a href="https://www.micab.co" rel="nofollow noopener noreferrer" target="_blank">MiCab systems Corp.</a>
            </footer>
          </main>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    sidebarState: store.navigation.sidebarState,
    sidebarPosition: store.navigation.sidebarPosition,
  };
}

export default withRouter(connect(mapStateToProps)(withStyles(s)(Layout)));

