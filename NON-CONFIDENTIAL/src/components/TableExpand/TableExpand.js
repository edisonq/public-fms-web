import React from 'react';
import {
  BootstrapTable,
  TableHeaderColumn,
} from 'react-bootstrap-table';

export default class BSTable extends React.Component {
  constructor(props){
    super(props);    
    
  }
    render() {
      console.log('nisulod sa tableexpand');
      console.log(this.props);
      if (this.props.data) {
        return (
          <BootstrapTable data={ this.props.data }>
            <TableHeaderColumn dataField='fieldA' isKey={ true }>Field A</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldB'>Field B</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldC'>Field C</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldD'>Field D</TableHeaderColumn>
          </BootstrapTable>);
      } else {
        return (<p>?</p>);
      }
    }
  }