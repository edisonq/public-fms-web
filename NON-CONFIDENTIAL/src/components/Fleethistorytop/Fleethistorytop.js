import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    ButtonGroup,
    ButtonDropdown,
    Col,
    Dropdown,
    DropdownMenu,
    DropdownToggle,
    DropdownItem,
    Row,
} from 'reactstrap';

import s from './Fleethistorytop.scss';

class Fleethistorytop extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    sidebarState: PropTypes.string.isRequired,
    sidebarPosition: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
      this.state = {
      };
  }

  toggleFive = () => {
    this.setState({
      dropdownOpenFive: !this.state.dropdownOpenFive,
    });
  };

  render() {
    return (
        <Row>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <a
              className="btn btn-inverse btn-lg btn-block"
              href="/app/issueworkorders"
              >
              <i className="fa fa-exclamation" />
              </a>
            </div>
            <div className="description">
              <strong>14</strong> Issue Work Orders
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <a
                className="btn btn-inverse btn-lg btn-block"
                href="/app/fleet/repair"
                >
                <i className="fa fa-wrench" />
                </a>
            </div>
            <div className="description">
              Repairs
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <a
                className="btn btn-inverse btn-lg btn-block"
                href="/app/fleet/inspect"
                >
                <i className="fa fa-search" />
              </a>
            </div>
            <div className="description">
              Inspect
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <a
                className="btn btn-inverse btn-lg btn-block"
                href="/app/fleet/fuel"
                >
                <i className="fa fa-tint" />
              </a>
            </div>
            <div className="description">
              Fuel
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <Button color="inverse" size="lg" block><i className="fa fa-money" /></Button>
            </div>
            <div className="description">
              Expense
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <a
                className="btn btn-inverse btn-lg btn-block"
                href="/app/fleet/history"
                >
                <i className="fa fa-history" />
              </a>
            </div>
            <div className="description">
              History
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
              <Button color="inverse" size="lg" block><i className="fa fa-tasks" /></Button>
            </div>
            <div className="description">
              Task
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          <div>
            <div className="icon">
            <a
                className="btn btn-inverse btn-lg btn-block"
                href="/app/fleet/parts"
                >
                <i className="fa fa-gears" />
              </a>
            </div>
            <div className="description">
              Parts
            </div>
          </div>
        </Col>
        <Col lg={1} md={4} sm={6}>
          
          <div>
            <div className="icon">
            <Button color="inverse" size="lg" block><i className="fa fa-tachometer" /></Button>
            </div>
            <div className="description">
              Odometer
            </div>
          </div>
        </Col>
        <Col lg={3} md={4} sm={6}>
          <div>
          <ButtonDropdown
              className="mr-xs"
              isOpen={this.state.dropdownOpenFive}
              toggle={this.toggleFive} 
              block
            >
            <Button block size="lg" color="primary">New</Button>
            <DropdownToggle caret color="primary" className="dropdown-toggle-split btn-lg" />
            <DropdownMenu>
              <DropdownItem>Record Accident</DropdownItem>
              <DropdownItem>Record Fluid Usage</DropdownItem>
              <DropdownItem>Meter</DropdownItem>
              <DropdownItem>Tire Assignment</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Last PM Setup</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Edit</DropdownItem>
              <DropdownItem>Delete</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>
          </div>
        </Col>
      </Row>
    );
  }
}

function mapStateToProps(store) {
//   return {
    // sidebarState: store.navigation.sidebarState,
    // sidebarPosition: store.navigation.sidebarPosition,
//   };
}

export default withStyles(s)(Fleethistorytop);
