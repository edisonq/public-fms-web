import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Progress, Alert, NavItem, NavLink } from 'reactstrap';
import { withRouter } from 'react-router-dom';

import s from './Sidebar.scss';
import LinksGroup from './LinksGroup/LinksGroup';
import { dismissAlert } from '../../actions/alerts';
import { changeActiveSidebarItem } from '../../actions/navigation';

class Sidebar extends React.Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    sidebarOpen: PropTypes.bool.isRequired
  };

  componentDidMount() {
    this.element.addEventListener('transitionend', () => {
      if (this.props.sidebarOpen) {
        this.element.classList.add(s.sidebarOpen);
      } else {
        this.element.classList.remove(s.sidebarOpen);
      }
    }, false);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.sidebarOpen !== this.props.sidebarOpen) {
      if (nextProps.sidebarOpen) {
        this.element.style.height = `${this.element.scrollHeight}px`;
      } else {
        this.element.style.height = `${this.element.scrollHeight}px`;
        this.element.classList.remove(s.sidebarOpen);
        setTimeout(() => {
          this.element.style.height = '';
        }, 0);
      }
    }
  }

  dismissAlert(id) {
    this.props.dispatch(dismissAlert(id));
  }

  render() {
    return (
      /* eslint-disable */
      <div className={s.root}>
        <nav className={`${s.sidebar} sidebar`}
             ref={(nav) => { this.element = nav; }}
        >
          <ul className={s.nav}>
            <LinksGroup header="Dashboard" headerLink="/app" iconName="fa-home" />
            <LinksGroup
              onActiveSidebarItemChange={() => this.props.dispatch(changeActiveSidebarItem('/app/fleet'))}
              isActive={this.props.activeItem === '/app/fleet'}
              header="Fleet"
              iconName="fa-cab"
              disable="true"
              headerLink="/app/fleet/all"
              childrenLinks={[
                {
                  name: 'All Fleet', link: '/app/fleet/all',
                },
                {
                  name: 'Overdue', link: '/app/fleet/overdue',
                },
                {
                  name: 'Soon due', link: '/app/fleet/soondue',
                },
                {
                  name: 'WO Assigned', link: '/app/fleet/woassigned',
                },
              ]}
            />
            <LinksGroup
              onActiveSidebarItemChange={() => this.props.dispatch(changeActiveSidebarItem('/app/statistics'))}
              isActive={this.props.activeItem === '/app/statistics'}
              header="Inventory"
              iconName="fa-cubes"
              headerLink="/app/inventory/parts"
              childrenLinks={[
                {
                  name: 'Parts', link: '/app/inventory/parts',
                },
                {
                  name: 'Fuel', link: '/app/inventory/fuel',
                },
                {
                  name: 'Tires', link: '/app/inventory/tires',
                },
                {
                  name: 'Equipments', link: '/app/inventory/equipments',
                },
                {
                  name: 'Expected parts', link: '/app/inventory/expectedparts',
                },
              ]}
            />
            <LinksGroup header="Issues" headerLink="/app/issues" iconName="fa-exclamation-triangle" />
            <LinksGroup
              onActiveSidebarItemChange={() => this.props.dispatch(changeActiveSidebarItem('/app/ui'))}
              isActive={this.props.activeItem === '/app/ui'}
              header="Rentals"
              iconName="fa-road"
              headerLink="/app/rentals/renttoown"
              childrenLinks={[
                {
                  name: 'Rent to own', link: '/app/rentals/renttoown',
                },
                {
                  name: 'Remittance', link: '/app/rentals/remittance',
                },
              ]}
            />
            <LinksGroup
              onActiveSidebarItemChange={() => this.props.dispatch(changeActiveSidebarItem('/app/components'))}
              isActive={this.props.activeItem === '/app/components'}
              header="Financials"
              iconName="fa-money"
              headerLink="/app/financials/purchasing"
              childrenLinks={[
                {
                  name: 'Purchasing', link: '/app/financials/purchasing',
                },
                {
                  name: 'Expenses', link: '/app/financials/expenses',
                },
                {
                  name: 'Deposits', link: '/app/financials/deposits',
                },
                {
                  name: 'Debts', link: '/app/financials/Debts',
                },
                {
                  name: 'Payroll', link: '/app/financials/payroll',
                },
                {
                  name: 'Invoices', link: '/app/financials/invoices',
                },
              ]}
            />
            <LinksGroup header="Work Orders" headerLink="/app/workorders" iconName="fa-dot-circle-o" />
            <LinksGroup header="Service Entries" headerLink="/app/serviceentries" iconName="fa-book" />
            <LinksGroup header="Legals" headerLink="/app/legals" iconName="fa-legal" />
            <LinksGroup
              onActiveSidebarItemChange={() => this.props.dispatch(changeActiveSidebarItem('/app/tables'))}
              isActive={this.props.activeItem === '/app/tables'}
              header="People"
              iconName="fa-users"
              headerLink="/app/people/employees"
              childrenLinks={[
                {
                  name: 'Employees', link: '/app/people/employees',
                },
                {
                  name: 'Customers', link: '/app/people/customers',
                },
                {
                  name: 'Vendors', link: '/app/people/vendors',
                },
              ]}
            />
            <LinksGroup header="Calendar" headerLink="/app/calendar" iconName="fa-calendar" />
            <LinksGroup header="Inspections" headerLink="/app/inspections" iconName="fa-eye" />
            <LinksGroup header="Geofencing" headerLink="/app/geofencing" iconName="fa-map-marker" />
          </ul>
          <h6 className={s.navTitle}>
            Labels
            <a className={s.actionLink}>
              <i className={`${s.glyphiconSm} glyphicon glyphicon-plus float-right`} />
            </a>
          </h6>
          <ul className={`${s.sidebarLabels} text-list`}>
            <NavItem>
              <NavLink href="#">
                <i className="fa fa-circle text-warning" />
                <span className={s.labelName}>My Recent</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">
                <i className="fa fa-circle text-gray" />
                <span className={s.labelName}>Starred</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">
                <i className="fa fa-circle text-danger" />
                <span className={s.labelName}>Background</span>
              </NavLink>
            </NavItem>
          </ul>
          <h6 className={s.navTitle}>
            Projects
          </h6>
          <div className={s.sidebarAlerts}>
            {this.props.alertsList.map(alert => // eslint-disable-line
              <Alert
                key={alert.id}
                className={s.sidebarAlert} color="transparent"
                isOpen={true} // eslint-disable-line
                toggle={() => {
                  this.dismissAlert(alert.id);
                }}
              >
                <span className="text-white fw-semi-bold">{alert.title}</span><br />
                <Progress className={`${s.sidebarProgress} progress-xs mt-1`} color={alert.color} value={alert.value} />
                <small>{alert.footer}</small>
              </Alert>,
            )}
          </div>
        </nav>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

export default withRouter(connect(mapStateToProps)(withStyles(s)(Sidebar)));
