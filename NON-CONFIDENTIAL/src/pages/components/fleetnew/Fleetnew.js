import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '../action/postActions';

class Fleetnew extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      description: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    const post = {
      name: this.state.name,
      description: this.state.description
    }

    // call action
    this.props.createPost(post);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
    
  render() {
    return (
      <div>
        <h1>Add Post</h1>
        <form onSubmit={this.onSubmit}>
          <div>
            <label>Name: </label>
            <input type="text" name="name" onChange={this.onChange} value={this.state.name} />
          </div>
          <div>
            <label>Description: </label>
            <input type="text" name="description" onChange={this.onChange} value={this.state.description}/>
          </div>
          <button type="submit">submit</button>
        </form>
      </div>
    )
  }
}

Fleetnew.propTypes =  {
  createPost: PropTypes.func.isRequired
}

export default connect(null, { createPost })(Fleetnew);