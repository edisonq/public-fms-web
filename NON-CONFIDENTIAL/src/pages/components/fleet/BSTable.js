import React, { Component } from 'react'

class BSTable extends Component {
  render() {
    if (this.props.data) {
        return (
          <BootstrapTable data={ this.props.data }>
            <TableHeaderColumn dataField='fieldA' isKey={ true }>Field A</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldB'>Field B</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldC'>Field C</TableHeaderColumn>
            <TableHeaderColumn dataField='fieldD'>Field D</TableHeaderColumn>
          </BootstrapTable>);
      } else {
        return (<p>?</p>);
      }
  }
}

export default BSTable;