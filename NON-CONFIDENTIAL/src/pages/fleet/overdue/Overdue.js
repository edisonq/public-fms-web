import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  ButtonGroup,
  ButtonDropdown,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Progress,
  Row,
  Col,
 
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import PropTypes  from 'prop-types';

// import ReactTable from 'react-table';
import { Link, withRouter } from 'react-router-dom';
import { reactTableData, reactBootstrapTableData } from './data';
import Fleettop from '../../../components/Fleettop';
import Widget from '../../../components/Widget';
import { fetchFleets } from "../../../actions/fleet";

import s from './Overdue.scss';

class Overdue extends React.Component {

  componentWillMount = () => {
    this.props.fetchFleets();
  }
  
  componentWillReceiveProps(nextProps) {
    // if (nextProps.newPost) {
    //   this.props.posts.unshift(nextProps.newPost);
    // }
  }

  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };
  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  render() {
    let fleetItems;
    
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
      expandRowBgColor: 'rgb(242, 255, 163)',
    };   

    if (this.props.fleets) {
       fleetItems =  this.props.fleets.map(fleet => {
        fleet.bodySerialEngineSerial = {
          "bodySerialNumber": fleet.bodySerialNumber,
          "engineSerialNumber": fleet.engineSerialNumber
        };
        fleet.bodyLTRFBPlateNumber = {
          "bodyNumber": fleet.bodyNumber,
          "LTRFBnumber": fleet.LTRFBnumber,
          "plateNumber": fleet.plateNumber
        };
        fleet.vehicleType = fleet.makerModel.type;
        fleet.vehicleMaker = fleet.makerModel.makerName;
        fleet.vehicleModel = fleet.makerModel.modelName;

        // begin: kwaon ang highest sa meter
        // ---
        fleet.maxMeterReading = {
          "mileage": 0,
          "dateRead": '',
          "automated": false
        };
        if (Object.keys(fleet.meterReading).length) {            
             fleet.maxMeterReading = {
              "mileage": fleet.meterReading[0].mileage,
              "dateRead": fleet.meterReading[0].dateRead,
              "automated": fleet.meterReading[0].automated
            };
            
            for (let i = 0, len=Object.keys(fleet.meterReading).length; i < len; i++) {
              let mileage = fleet.meterReading[i].mileage;
              let dateRead = fleet.meterReading[i].dateRead;
              let automated = fleet.meterReading[i].automated;
              if (mileage > fleet.maxMeterReading.mileage) {
                fleet.maxMeterReading.mileage = mileage;
                fleet.maxMeterReading.dateRead = dateRead;
                fleet.maxMeterReading.automated = automated;
              } 
            }
        } 
        // --
        // end of kwaon ang highest sa meter

        // begin: compute ang status sa next maintenance
        let progressStatus;
        let typeStatus;
        fleet.dueStatus = {
          type: "info",
          progress: 0
        };
        const partsVehicleLength = Object.keys(fleet.partsVehicle).length;
        
        if (partsVehicleLength) {
          for (let i = 0; i < partsVehicleLength; i++) {
            let percentageSecondsDue = 0;
            let percentageDistanceDue = 0;
            // compare current time and dateEstimatedExpiration
            if ( Math.floor((new Date().getTime())/1000) < fleet.partsVehicle[i].dateEstimatedExpiration) {
              // compute sa percentage sa time sa expiration
              // START guide ra ni:
              // var difference = date1.getTime() - date2.getTime();
              // var daysDifference = Math.floor(difference/1000/60/60/24);
              // difference -= daysDifference*1000*60*60*24
              
              // var hoursDifference = Math.floor(difference/1000/60/60);
              // difference -= hoursDifference*1000*60*60
              
              // var minutesDifference = Math.floor(difference/1000/60);
              // difference -= minutesDifference*1000*60

              // var secondsDifference = Math.floor(difference/1000);
              // END guide ra ni:
              const currentAndAttachedDifferenceSeconds = Math.floor((new Date().getTime())/1000)  - fleet.partsVehicle[i].dateAttached;
              const currentAndEstimatedDifferenceSeconds = fleet.partsVehicle[i].dateEstimatedExpiration - Math.floor((new Date().getTime())/1000);
              percentageSecondsDue = (currentAndAttachedDifferenceSeconds/(currentAndAttachedDifferenceSeconds+currentAndEstimatedDifferenceSeconds)) * 100;
              // console.log(fleet.partsVehicle[i].dateEstimatedExpiration);
              // console.log(currentAndEstimatedDifferenceSeconds);
              // console.log(currentAndAttachedDifferenceSeconds);
              // console.log(percentageSecondsDue);            
            } else {
              percentageSecondsDue = 100;
            }

            // compute sa percentage sa mileage 
            // console.log(fleet.dueStatus);
            if (fleet.maxMeterReading.mileage < fleet.partsVehicle[i].estimatedDistanceLimit){
              percentageDistanceDue = ((fleet.maxMeterReading.mileage - fleet.partsVehicle[i].currentDistanceAttached)/(fleet.partsVehicle[i].estimatedDistanceLimit - fleet.partsVehicle[i].currentDistanceAttached))*100;
            } else {
              percentageDistanceDue = 100;
            }

            // then ang mas highest percentage mao ang ipasa sa display
            progressStatus = fleet.dueStatus.progress;
            progressStatus = (progressStatus < percentageSecondsDue) ? percentageSecondsDue : progressStatus;
            progressStatus = (progressStatus < percentageDistanceDue) ? percentageDistanceDue : progressStatus;
            if (progressStatus < 20) {
              typeStatus = "info";
            } else if (progressStatus < 40) {
              typeStatus = "bar-gray-light";
            } else if (progressStatus < 60) {
              typeStatus = "success";
            } else if (progressStatus < 80) {
              typeStatus = "warning";
            } else if (progressStatus < 100) {
              typeStatus = "danger"
            } else {
              typeStatus = "";
            }

            fleet.dueStatus = {
              "progress": progressStatus,
              "type": typeStatus
            };

          }
        }
        // end: compute ang status sa next maintenance

        return fleet;
      });  
    } else {
       fleetItems = this.props.fleets;  
    }
    

    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true,  // click to select, default is false
      clickToExpand: true  // click to expand row, default is false
    };

    function bodyLTRFBPlateNumberFormatter(cell) {
      return (
        <div>
          <small>
            {cell.bodyNumber}
          </small>
          <br/>
          <small>
            {cell.LTRFBnumber}
          </small>
          <br/>
          <small>
            <span className="fw-semi-bold">plate:</span>&nbsp;{cell.plateNumber}
          </small>
        </div>
      );
    }

    function makerModelFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Maker:</span>&nbsp;{cell.makerName}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Model:</span>&nbsp;{cell.modelName}
          </small>
        </div>
      );
    }

    function bodyengineFormatter(cell) {
      return (
        <div>
          <small>
            {cell.bodySerialNumber}
          </small>
          <br/>
          <small>
            {cell.engineSerialNumber}
          </small>
        </div>
      )
    }

    function odometerFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">{((cell.mileage) ? cell.mileage : '')}</span>
          </small>
          <br/>
          <small>
            {((cell.dateRead) ? new Date(cell.dateRead*1000).toString() : '') }
          </small>
        </div>
      )
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.dueStatus.progress - b.dueStatus.progress;
      }
      return b.dueStatus.progress - a.dueStatus.progress;
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/overdue">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Overdue</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Overdue</span></h2>
        
        <Widget title={<h4>Showing <span className="fw-semi-bold">Overdue</span></h4>} settings close>
        <Fleettop />
          <BootstrapTable 
            data={fleetItems}
            version="4" 
            pagination
            options={options} 
            expandableRow={ this.isExpandableRow }
            expandComponent={ this.expandComponent }
            expandColumnOptions={ { expandColumnVisible: true } }
            selectRow={selectRow}
            search >
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey hidden>ID</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="plateNumber" hidden>plateNumber</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="LTRFBnumber" hidden>LTRFBnumber</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="bodyNumber" hidden>bodyNumber</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="engineSerialNumber" hidden>engineSerialNumber</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="bodySerialNumber" hidden>bodySerialNumber</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="vehicleType" hidden>vehicleType</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="vehicleModel" hidden>vehicleModel</TableHeaderColumn>
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="vehicleMaker" hidden>vehicleMaker</TableHeaderColumn>
            <TableHeaderColumn dataField="type" hidden>Type</TableHeaderColumn>
            <TableHeaderColumn dataField="bodyLTRFBPlateNumber" datasort dataFormat={bodyLTRFBPlateNumberFormatter}>Body Number<br/>LTRFB Number<br/>Plate Number</TableHeaderColumn>
            <TableHeaderColumn dataField="bodySerialEngineSerial" dataSort dataFormat={bodyengineFormatter}>Body Serial number<br/>Engine number</TableHeaderColumn>
            <TableHeaderColumn dataField="makerModel" dataSort dataFormat={makerModelFormatter}>Maker<br/>Model</TableHeaderColumn>
            <TableHeaderColumn dataField="maxMeterReading" dataFormat={odometerFormatter} dataSort>Odometer</TableHeaderColumn>
            <TableHeaderColumn dataField="status" dataSort>Status</TableHeaderColumn>
            <TableHeaderColumn dataField="dueStatus"  dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Due Status</TableHeaderColumn>
          </BootstrapTable>
        </Widget>

      </div>
    );
  }

}

Overdue.propTypes = {
  fetchFleets: PropTypes.func.isRequired,
  // fleets: PropTypes.array.isRequirednpm 
  // newPost: PropTypes.object
}

const mapStateToProps = state => ({
  fleets: state.fleets.items,
  fetchFleets: PropTypes.func.isRequired,
  reactTable: reactTableData(),
  reactBootstrapTable: reactBootstrapTableData(),
  // newPost: state.posts.item
});

const styledComponent = withStyles(s)(Overdue);
export default withRouter(connect(mapStateToProps, {fetchFleets})(styledComponent));

