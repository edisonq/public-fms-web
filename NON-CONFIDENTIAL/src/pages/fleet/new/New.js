import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  Row,
  TabContent,
  TabPane,
  NavLink,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import PropTypes  from 'prop-types';
import { connect } from 'react-redux';


import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, withRouter, browserHistory, Route } from 'react-router'; 
import { Link, Redirect } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import TextareaAutosize from 'react-autosize-textarea';
import Widget from '../../../components/Widget';
import s from './New.scss';
import { fetchFleets, createFleet } from "../../../actions/fleet";
import { reactTableData, reactBootstrapTableData } from './data';

class New extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      bodyNumber: "",
      LTRFBnumber: "",
      plateNumber: "",
      bodySerialNumber: "",
      engineSerialNumber: "",
      status: "",
      makerModel: "",
      activeFirstTab: 'mainTab',
      selectGroupData: [{
        id: 'on-garage',
        text: 'On Garage'
      }, {
        id: 'out',
        text: 'Out for operation'
      }, {
        id: 'repair or maintenance',
        text: 'Repair or Maintenance'
      }, {
        id: 'disposed',
        text: 'disposed'
      }],
      selectGroupDataMakerTypeModel: [{
        id: '5b5aac66023eed3cdb803266',
        text: 'Nissan',
        children: [{
          id: '5b5aac66023eed3cdb803266', text: 'Almera Sedan AT',
        }, {
          id: '5b5aac66023eed3cdb803266', text: 'Almera Sedan MT 1.5',
        }, {
          id: '5b5aac66023eed3cdb803266', text: 'Almera Sedan MT 1.3',
        }],
      }, {
        id: '5b5aac66023eed3cdb803266',
        text: 'Toyota',
        children: [{
          id: '5b5aac66023eed3cdb803266', text: 'Altis Sedan MT',
        }, {
          id: '5b5aac66023eed3cdb803266', text: 'Altis Van AT',
        }],
      }],
      selectDefaultData: [{
        id: 'Magellanic', text: 'Large Magellanic Cloud',
      }, {
        id: 'Andromeda', text: 'Andromeda Galaxy',
      }, {
        id: 'Sextans', text: 'Sextans A',
      }]
    };
    // on-garage', 'out', 'repair or maintenance', 'disposed'
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    const post = {
      bodyNumber: this.state.bodyNumber,
      LTRFBnumber: this.state.LTRFBnumber,
      plateNumber: this.state.plateNumber,
      bodySerialNumber: this.state.bodySerialNumber,
      engineSerialNumber: this.state.engineSerialNumber,
      status: this.state.status,
      makerModel: this.state.makerModel
    }

    // call action
    this.props.createFleet(post);
    this.props.history.push(`/app/fleet/all`);///store/${this.storeInput.value}`);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  toggleFirstTabs(tab) {
    if (this.state.activeFirstTab !== tab) {
      this.setState({
        activeFirstTab: tab,
      });
    }
  }

  render() {
    
    console.log(this.props.location.state);
    return (
    <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/overdue">Fleet</Link></BreadcrumbItem>
          <BreadcrumbItem active>New fleet</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">New</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
              className="btn btn-inverse btn-lg"
              to="/app/fleet/all"
              >
              <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
          <Widget title={<h4>Add <span className="fw-semi-bold">new vehicle</span></h4>}>
          <Nav  tabs className="tabs-right flex-column">
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'mainTab' })}
                onClick={() => {
                  this.toggleFirstTabs('mainTab');
                }}
              >
                <span>Main</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'specificationTab' })}
                onClick={() => {
                  this.toggleFirstTabs('specificationTab');
                }}
              >
                <span>Specification</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'warrantyTab' })}
                onClick={() => {
                  this.toggleFirstTabs('warrantyTab');
                }}
              >
                <span>Warranty or Miscellaneous </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'purchaseTab' })}
                onClick={() => {
                  this.toggleFirstTabs('purchaseTab');
                }}
              >
                <span>Purchase</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'loanTab' })}
                onClick={() => {
                  this.toggleFirstTabs('loanTab');
                }}
              >
                <span>Loan or Lease </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'insuranceTab' })}
                onClick={() => {
                  this.toggleFirstTabs('insuranceTab');
                }}
              >
                <span>Insurance </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'tiresTab' })}
                onClick={() => {
                  this.toggleFirstTabs('tiresTab');
                }}
              >
                <span>Tires </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'imagesTab' })}
                onClick={() => {
                  this.toggleFirstTabs('imagesTab');
                }}
              >
                <span>Images </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'attachmentsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('attachmentsTab');
                }}
              >
                <span>Attachments </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'notificationTab' })}
                onClick={() => {
                  this.toggleFirstTabs('notificationTab');
                }}
              >
                <span>Notifications </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'expensesTab' })}
                onClick={() => {
                  this.toggleFirstTabs('expensesTab');
                }}
              >
                <span>Expenses </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'notesTab' })}
                onClick={() => {
                  this.toggleFirstTabs('notesTab');
                }}
              >
                <span>Notes </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'settingsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('settingsTab');
                }}
              >
                <span>Settings </span>
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeFirstTab}>
            <TabPane tabId="mainTab" >
              <FormGroup>
                <Form onSubmit={this.onSubmit}>
                <legend className="section">Vehicle information</legend>
                    <FormGroup row>
                      <Label for="bodynumber" md={4} className="text-md-right">
                        Body number:
                      </Label>
                      <Col md={7}>
                        <Input type="text" id="bodyNumber" name="bodyNumber" placeholder="body number"  onChange={this.onChange} value={this.state.bodyNumber} />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label for="LTRFBnumber" md={4} className="text-md-right">
                        LTRFB number:
                      </Label>
                      <Col md={7}>
                        <Input type="text" id="LTRFBnumber" name="LTRFBnumber" placeholder="LTRFB number"  onChange={this.onChange} value={this.state.LTRFBnumber} />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label for="plateNumber" md={4} className="text-md-right">
                        Plate number:
                      </Label>
                      <Col md={7}>
                        <Input type="text" id="plateNumber" name="plateNumber" placeholder="Plate number"  onChange={this.onChange} value={this.state.plateNumber} />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label for="bodySerialNumber" md={4} className="text-md-right">
                        Body serial number:
                      </Label>
                      <Col md={7}>
                        <Input type="text" id="bodySerialNumber" name="bodySerialNumber" placeholder="Body serial number"   onChange={this.onChange} value={this.state.bodySerialNumber}  />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label for="engineserialnumber" md={4} className="text-md-right">
                        Engine serial number:
                      </Label>
                      <Col md={7}>
                        <Input type="text" id="engineSerialNumber" name="engineSerialNumber" placeholder="Engine serial number" onChange={this.onChange} value={this.state.engineSerialNumber}   />
                      </Col>
                    </FormGroup>
                    <FormGroup row> {/* todo: fix group select */}
                      <Label md={4} className="text-md-right" for="status">Status</Label>
                      <Col md="6" className={s.select2}>
                        <Select2 id="status" name="status"
                          data={this.state.selectGroupData}
                          onChange={this.onChange} value={this.state.status}  
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup row> {/* todo: fix group select */}
                      <Label md={4} className="text-md-right" for="makername">Maker, Type, and model</Label>
                      <Col md="6" className={s.select2}>
                        <Select2 id="makername" name="makerModel"
                          data={this.state.selectGroupDataMakerTypeModel}
                          onChange={this.onChange} value={this.state.makerModel}  
                        />
                      </Col>
                    </FormGroup>
                
              <FormGroup row className={s.submitBlock}>
                  <Label md={4} />
                  <Col md={7}>
                    <Button color="primary" type="submit" className="mr-xs">Add Vehicle</Button>
                    <Button color="inverse">Cancel</Button>
                  </Col>
                </FormGroup>
                </Form>
              </FormGroup>
            </TabPane>
            <TabPane tabId="specificationTab">
              specification content here
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="warrantyTab">
              Warranty or Miscellaneous
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="purchaseTab">
              Purchase
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="loanTab">
              Loan
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="insuranceTab">
              Insurance
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="tiresTab">
              Tires
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="imagesTab">
              Images
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="attachmentsTab">
              Attachments
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="notificationTab">
              Notification
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="expensesTab">
              Expenses
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="notesTab">
              Notes
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
            <TabPane tabId="settingsTab">
              Settings
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </TabPane>
          </TabContent>
          
        </Widget>
    </div>);
  }
}

// export default withStyles(s)(New);
New.propTypes = {
    fetchFleets: PropTypes.func.isRequired,
    // fleets: PropTypes.array.isRequired,
    // newPost: PropTypes.object
}

const mapStateToProps = state => ({
// fleets: state.fleets.items,
// fetchFleets: PropTypes.func.isRequired,
reactTable: reactTableData(),
reactBootstrapTable: reactBootstrapTableData(),
// newPost: state.posts.item,
});

const styledComponent = withStyles(s)(New);
export default withRouter(connect(mapStateToProps, {fetchFleets, createFleet})(styledComponent));
