import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonToolbar,
  ButtonGroup,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Row,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';
import PropTypes  from 'prop-types';

import { Router, browserHistory } from 'react-router';
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';

import { fetchFleetParts } from "../../../actions/fleetparts";
import { reactTableData, reactBootstrapTableData } from './data';
import Widget from '../../../components/Widget';
import s from './Fleetparts.scss';
import Lightbox from 'react-images';
import i5 from '../../../images/5.jpg';
import i6 from '../../../images/6.jpg';
import i7 from '../../../images/7.jpg';
import i8 from '../../../images/8.jpg';
import i9 from '../../../images/9.jpg';
import i10 from '../../../images/10.jpg';
import i11 from '../../../images/11.jpg';
import i12 from '../../../images/12.jpg';
// import BSTable from '../../../components/TableExpand';

class Fleetparts extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentImage: 0,
      lightboxIsOpen: false,
      images: [{ src: i5 }, { src: i9 }, { src: i6 }, { src: i8 }, { src: i7 }],
      theme: {
        arrow: {
          ':focus': {
            outline: 0,
          },
        },
        close: {
          ':focus': {
            outline: 0,
          },
        },
      },
      expanding: [2],
      reactTable: reactTableData(),
      reactBootstrapTable: reactBootstrapTableData(),
    };

      this.closeLightbox = this.closeLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
      this.gotoImage = this.gotoImage.bind(this);
      this.handleClickImage = this.handleClickImage.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
  
  }

  componentWillMount = () => {
    this.props.fetchFleetParts();
  }

  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  openLightbox = (index, event) => {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  };

  gotoPrevious = () => {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  };

  gotoImage = (index) => {
    this.setState({
      currentImage: index,
    });
  };

  gotoNext = () => {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  };

  closeLightbox = () => {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  };

  handleClickImage = () => {
    if (this.state.currentImage === this.state.images.length - 1) return;

    this.gotoNext();
  };

  isExpandableRow = () => {
    console.log('expand table');
    return true;
  };

  expandComponent = (row) => {
    console.log('ni expand table');
    return (
      // <BSTable data={ row.expand } />
      <div>
        just another test
      </div>
    );
  };

  render() {
    console.log(this.props.fleetparts);
    let fleetpartsData;
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
      expandRowBgColor: 'rgba(249, 249, 249,.4)',
      expanding: this.state.expanding,
    };

    if (this.props.fleetparts) {
      fleetpartsData =  this.props.fleetparts.map(fleetpart => {
        fleetpart.estimation = {
          "dateEstimatedExpiration": fleetpart.dateEstimatedExpiration,
          "estimatedDistanceLimit": fleetpart.estimatedDistanceLimit
        };
        fleetpart.attachinformation = {
          "currentDistanceAttached": fleetpart.currentDistanceAttached,
          "dateAttached": fleetpart.dateAttached,
          "attached": fleetpart.attached
        };

        // begin: compute ang status sa next maintenance
        let percentageSecondsDue = 0;
        let percentageDistanceDue = 0;
        // compare current time and dateEstimatedExpiration
        if ( Math.floor((new Date().getTime())/1000) < fleetpart.dateEstimatedExpiration) {
          
        }

        // end: compute ang status sa next maintenance
      });
      
    }

    function dateFormatter(cell) {
      return (
        <div>
          <span>
            {console.log(cell)}
            {((cell) ? new Date(cell*1000).toString() : '') }
          </span>
        </div>
      );
    }

    function attachedFormatter(cell) {
      return (
        <div>
          <small>
            {((cell.dateAttached) ? new Date(cell.dateAttached*1000).toString() : '') }
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Odometer:</span>&nbsp;{((cell.currentDistanceAttached) ? cell.currentDistanceAttached : '') }
          </small>
          <br/>
          <small>
          <span className="fw-semi-bold">still attached?</span>&nbsp;{((cell.attached) ? cell.attached : '') }
          </small>
        </div>
      );
    }

    function estimationFormatter(cell) {
      return (
        <div>
          <small>
            {((cell.dateEstimatedExpiration) ? new Date(cell.dateEstimatedExpiration*1000).toString() : '') }
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Distance limit:</span>&nbsp;{((cell.estimatedDistanceLimit) ? cell.estimatedDistanceLimit : '') }
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/all">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Parts</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Parts</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
              className="btn btn-inverse btn-lg"
              to="/app/fleet/all"
              >
              <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            <h3 className="text-right">Q076-D5B Cat Dozer</h3>
            <h4 className="text-right">Hours 6,833 - Mileage: 3,345</h4>
          </Col>
        </Row>
        <div className={s.galleryWidget}>
          <Row className="thumbnails">
            <Col md="2">
              <a
                href="#"
                onClick={(e) => { this.openLightbox(0, e); }}
              >
                <img src={i5} alt="" className="img-thumbnail" />
              </a>
            </Col>
            <Col md="2">
              <a 
                    href="#"
                    onClick={(e) => { this.openLightbox(1, e); }}
                  >
                    <img src={i9} alt="" className="img-thumbnail" />
              </a>
            </Col>
            <Col md="2">
              <a 
                href="#"
                onClick={(e) => { this.openLightbox(2, e); }}
              >
                <img src={i6} alt="" className="img-thumbnail" />
              </a>
            </Col>
            <Col md="2">
              <a 
                href="#"
                onClick={(e) => { this.openLightbox(3, e); }}
              >
                <img src={i8} alt="" className="img-thumbnail" />
              </a>
            </Col>
            <Col md="2">
              <a 
                href="#"
                onClick={(e) => { this.openLightbox(4, e); }}
              >
                <img src={i7} alt="" className="img-thumbnail" />
              </a>     
            </Col>
              
              <Lightbox
                currentImage={this.state.currentImage}
                images={this.state.images}
                isOpen={this.state.lightboxIsOpen}
                onClickPrev={this.gotoPrevious}
                onClickNext={this.gotoNext}
                onClose={this.closeLightbox}
                onClickImage={this.handleClickImage}
                onClickThumbnail={this.gotoImage}
                backdropClosesModal
                enableKeyboardInput
                theme={this.state.theme}
              />
          </Row>
        </div>
        <Row className="text-right">
          <ButtonToolbar className="float-right">
            <Button color="inverse">Add Parts</Button>
            <Button color="primary" disabled>Edit</Button>
            <Button color="primary" disabled>Delete</Button>
          </ButtonToolbar>
          <br/><br/>
        </Row>

        <Widget settings close>
          <BootstrapTable 
            data={this.props.fleetparts}
            expandColumnOptions={ { expandColumnVisible: true } }
            expandableRow={ this.isExpandableRow }
            expandComponent={ this.expandComponent }
            version="4" 
            pagination 
            options={options} 
            search
          >
            <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
            <TableHeaderColumn dataField="estimation" dataFormat={estimationFormatter} dataSort>Estimated<br/>Date of Expiration<br/>Distance</TableHeaderColumn>
            <TableHeaderColumn dataField="attachinformation" dataFormat={attachedFormatter} dataSort>Attached<br/>Date of Attachment<br/>Odometer<br/>Status</TableHeaderColumn>
            <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="dateEstimatedExpiration" dataSort sortFunc={dateSortFunc} dataFormat={dateFormatter}>Status</TableHeaderColumn>
          </BootstrapTable>
        </Widget>
       
      </div>
    );
  }

}

Fleetparts.propTypes = {
  fetchFleetParts: PropTypes.func.isRequired
}



const mapStateToProps = state => ({
  // fleetPart: state.fleetParts.items,
  fleetparts: state.fleetparts.items,
  fetchFleetParts: PropTypes.func.isRequired,
  reactTable: reactTableData(),
  reactBootstrapTable: reactBootstrapTableData(),
  // newPost: state.posts.item
});


const styledComponent = withStyles(s)(Fleetparts);
export default withRouter(connect(mapStateToProps, {fetchFleetParts})(styledComponent));