import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, browserHistory } from 'react-router'; 
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import TextareaAutosize from 'react-autosize-textarea';

import Widget from '../../../components/Widget';
import s from './Fleetfuel.scss';

class Fleetfuel extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isDatePickerOpen: false,
      dropdownOpenTwo: false,
      dropdownOpen: false,
      activeFirstTab: 'optionalTab',      
      selectFuelTypeData: [{
        id: 'Diesel', text: 'Diesel',
      }, {
        id: 'Gas', text: 'Gas',
      }, {
        id: 'Off Road Diesel', text: 'Off Road Diesel',
      }],
      defaultSelectFuelTypeVal: 'Gas',
      selectVendorData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname Lastname',
      }, {
        id: '003', text: 'Firstname Lastname',
      }],
      defaultSelectVendorVal: '001',
      selectDriverData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectDriverVal: '002',
      selectCostcenterData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectCostcenterVal: '003',
      selectStocktankData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectStocktankVal: '003',
      selectStocktankData: [{
        id: '001', text: 'Tank 1',
      }, {
        id: '002', text: 'Tank 2',
      }, {
        id: '003', text: 'Tank 3',
      }],
      defaultSelectStocktankVal: '001',
      selectBrandData: [{
        id: '001', text: 'Petron',
      }, {
        id: '002', text: 'Shell',
      }, {
        id: '003', text: 'PTT',
      }],
      defaultSelectBrandVal: '001',
      selectLocationData: [{
        id: '001', text: 'Lawaan',
      }, {
        id: '002', text: 'Dulju',
      }, {
        id: '003', text: 'Pit-os',
      }],
      defaultSelectLocationVal: '001',
      selectLocationData: [{
        id: '001', text: 'Lawaan',
      }, {
        id: '002', text: 'Dulju',
      }, {
        id: '003', text: 'Pit-os',
      }],
      defaultSelectLocationVal: '001',
    };
  }
  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  toggleTwo = () => {
    this.setState({
      dropdownOpenTwo: !this.state.dropdownOpenTwo,
    });
  };

  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  toggleFirstTabs = (tab) => {
    if (this.state.activeFirstTab !== tab) {
      this.setState({
        activeFirstTab: tab,
      });
    }
  };  

  render() {
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
    };

    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/overdue">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Fuel</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Fuel</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
                className="btn btn-inverse btn-lg"
                to="/app/fleet/overdue"
                >
                <i className="glyphicon glyphicon-arrow-left" /> CANCEL
              </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            &nbsp;
          </Col>
          <Col lg={4}  className="float-center">
            &nbsp;
          </Col>
          <Col lg={4} className="float-right">
            &nbsp;
          </Col>
        </Row>
        <Row>
          <Col lg={8}>
            <Widget title={<h4>Inspection Items</h4>} refresh close>
              <Row>
                &nbsp;
              </Row>
              
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'optionalTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('optionalTab');
                    }}
                  >
                    <span>Optional Details</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'miscTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('miscTab');
                    }}
                  >
                    <span>Miscellaneous</span>
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeFirstTab}>
                <TabPane tabId="optionalTab">
                  <FormGroup row>
                    <Label md="2" for="selectFueltype">Fuel type:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultFuelTypeVal}
                        data={this.state.selectFuelTypeData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Vendor:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectVendorVal}
                        data={this.state.selectVendorData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Driver:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectDriverVal}
                        data={this.state.selectDriverData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Cost Center:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectCostcenterVal}
                        data={this.state.selectCostcenterData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Stock Tank:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectStocktankVal}
                        data={this.state.selectStocktankData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Brand:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectBrandVal}
                        data={this.state.selectBrandData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Location:</Label>
                    <Col md="10">
                      <Select2
                        className={s.select2}
                        value={this.state.defaultSelectLocationVal}
                        data={this.state.selectLocationData}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label md="2" for="selectVendor">Notes:</Label>
                    <Col md="10">
                      <TextareaAutosize
                        rows={3} id="elastic-textarea"
                        placeholder="Try to add few new lines.."
                        className={`form-control ${s.autogrow} transition-height input-transparent`}
                      />
                    </Col>
                  </FormGroup>
                </TabPane>
                <TabPane tabId="miscTab">
                  <div className={s.tabPicture}>
                    <i className="fa fa-picture-o" />
                  </div>
                </TabPane>
              </TabContent>               
            </Widget>
            
          </Col>
          <Col lg={4} className="float-right text-md-left">
            <Row>
              <Col lg={6}>
                <Button color="inverse" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save and Next</Button>
              </Col>
              <Col lg={6}>
                <Button color="primary" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save</Button>
              </Col>
            </Row>
            <Widget  refresh close>
              <FormGroup row>
                <Label md="4" for="selectFuelType">Equipment:</Label>
                <Col md="7">
                  <Select2
                    block
                    className={s.select2}
                    value={this.state.defaultSelectFueltypeVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRequested">Fill:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Liters:</Label>
                <Col md="7">
                  <Input type="text" id="normal-field" placeholder="May have placeholder" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Price per liter:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Total Cost:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup  row>
              <Label md="4" for="dateScheduled">Date:&nbsp;</Label>
              <Col md="7">
                <Datetime 
                  id="dateScheduled"
                  open={this.state.isDatePickerOpen}
                  viewMode="days" timeFormat={false}
                  inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
                />
                <InputGroupAddon md="4" onClick={() => { this.refDatePicker.focus(); }}>
                  <i className="glyphicon glyphicon-th" />
                </InputGroupAddon>
              </Col>
              </FormGroup>
              <a
                className="btn btn-inverse  btn-block"
                href="/app/issueworkorders"
              >
              <i className="fa fa-wrench" />&nbsp;&nbsp;Request Repair
              </a>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }

}

function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

// export default withStyles(s)(Fleetfuel);
export default withRouter(connect(mapStateToProps)(withStyles(s)(Fleetfuel)));