import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, browserHistory } from 'react-router'; 
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import TextareaAutosize from 'react-autosize-textarea';

import Widget from '../../../components/Widget';
import s from './Fleettask.scss';

class Fleettask extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      
    };
  }
  

  render() {
    // const options = {
    //   sizePerPage: 10,
    //   paginationSize: 3,
    //   sizePerPageDropDown: this.renderSizePerPageDropDown,
    //   searchField: this.createCustomSearchField,
    // };

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/overdue">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Task</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Task</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
                className="btn btn-inverse btn-lg"
                to="/app/fleet/overdue"
                >
                <i className="glyphicon glyphicon-arrow-left" /> CANCEL
              </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            &nbsp;
          </Col>
          <Col lg={4}  className="float-center">
            &nbsp;
          </Col>
          <Col lg={4} className="float-right">
            &nbsp;
          </Col>
        </Row>
        <Widget>
          <FormGroup>
            <Form>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Vehicle:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Date:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Expense:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Cost:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Extended Cost:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="bodynumber" md={4} className="text-md-right">
                  Notes:
                </Label>
                <Col md={7}>
                  <Input type="text" id="bodynumber" placeholder="body number" />
                </Col>
              </FormGroup>
            </Form>
          </FormGroup>
        </Widget>
      </div>
    );
  }

}

function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

// export default withStyles(s)(Fleettask);
export default withRouter(connect(mapStateToProps)(withStyles(s)(Fleettask)));