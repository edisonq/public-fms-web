import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import PropTypes  from 'prop-types';
import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, browserHistory } from 'react-router'; 
import { Link, withRouter, Redirect } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import TextareaAutosize from 'react-autosize-textarea';

import { createExpenseFleet } from "../../../actions/fleetexpense";

import Widget from '../../../components/Widget';
import s from './Fleetexpense.scss';
import configuration from "../../../config";

class Fleetexpense extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      expenseName: "",
      expenseDescription: "",
      expenseDate : "",
      distanceTravelled: 0.0,
      cost: "",
      extendedCost: "",
      notes: "",
      vehicle: "",
      partsUsed: "",
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    const post = {
      expenseName: this.state.expenseName,
      expenseDescription: this.state.expenseDescription,
      expenseDate : this.state.expenseDate,
      distanceTravelled: 0,
      cost: this.state.cost,
      extendedCost: this.state.extendedCost,
      notes: this.state.notes,
      vehicle: this.state.vehicle,
      // partsUsed: this.state.partsUsed
    }

    // call action
    this.props.createExpenseFleet(post);
    this.props.history.push(`/app/fleet/all`);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }  

  render() {
    // const options = {
    //   sizePerPage: 10,
    //   paginationSize: 3,
    //   sizePerPageDropDown: this.renderSizePerPageDropDown,
    //   searchField: this.createCustomSearchField,
    // };

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/overdue">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Expense</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Expense</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
                className="btn btn-inverse btn-lg"
                to="/app/fleet/all"
                >
                <i className="glyphicon glyphicon-arrow-left" /> CANCEL
              </Link>
            {/* <Button color="inverse" size="lg"  onClick={this.onBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            &nbsp;
          </Col>
          <Col lg={4}  className="float-center">
            &nbsp;
          </Col>
          <Col lg={4} className="float-right">
            &nbsp;
          </Col>
        </Row>
        <Widget>
          <FormGroup>
            <Form  onSubmit={this.onSubmit}>
              <FormGroup row>
                <Label for="vehicle" md={4} className="text-md-right">
                  Vehicle:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" name="vehicle" id="vehicle" placeholder="Vehicle id" value={this.state.vehicle}/>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="expenseDate" md={4} className="text-md-right">
                  Date:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" id="expenseDate" name="expenseDate" placeholder="Expense date"  value={this.state.expenseDate} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="expenseName" md={4} className="text-md-right">
                  Expense:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" id="expenseName" name="expenseName" placeholder="Expense name"  value={this.state.expenseName}/>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="cost" md={4} className="text-md-right">
                  Cost:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" id="cost" name="cost" placeholder="cost"  value={this.state.cost} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="extendedCost" md={4} className="text-md-right">
                  Extended Cost:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" id="extendedCost" name="extendedCost" placeholder="Extended Cost"  value={this.state.extendedCost} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="notes" md={4} className="text-md-right">
                  Notes:
                </Label>
                <Col md={7}>
                  <Input  onChange={this.onChange} type="text" id="notes" name="notes" placeholder="Notes"  value={this.state.notes} />
                </Col>
              </FormGroup>
              <FormGroup row className={s.submitBlock}>
                <Label md={4} />
                <Col md={7}>
                  <Button color="primary" type="submit" className="mr-xs">Save Changes</Button>
                  <Button color="inverse">Cancel</Button>
                </Col>
              </FormGroup>
            </Form>
          </FormGroup>
        </Widget>
      </div>
    );
  }

}

Fleetexpense.propTypes =  {
  createExpenseFleet: PropTypes.func.isRequired
}

function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

// export default withStyles(s)(Fleetexpense);
export default withRouter(connect(mapStateToProps, {createExpenseFleet})(withStyles(s)(Fleetexpense)));