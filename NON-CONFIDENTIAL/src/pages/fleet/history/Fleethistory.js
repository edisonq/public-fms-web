import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, browserHistory } from 'react-router';
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import TextareaAutosize from 'react-autosize-textarea';
import { reactTableData, reactBootstrapTableData } from './data';

import Widget from '../../../components/Widget';
import s from './Fleethistory.scss';

import Lightbox from 'react-images';
import i5 from '../../../images/5.jpg';
import i6 from '../../../images/6.jpg';
import i7 from '../../../images/7.jpg';
import i8 from '../../../images/8.jpg';
import i9 from '../../../images/9.jpg';
import i10 from '../../../images/10.jpg';
import i11 from '../../../images/11.jpg';
import i12 from '../../../images/12.jpg';


class Fleethistory extends React.Component {

  constructor(props) {
    super(props);
    this.toggleFirstTabs = this.toggleFirstTabs.bind(this);

    this.state = {
      activeFirstTab: 'maintenanceTab',
      isDatePickerOpen: false,
      dropdownOpenTwo: false,
      dropdownOpen: false,
      reactTable: reactTableData(),
      reactBootstrapTable: reactBootstrapTableData(),
      selectFuelTypeData: [{
        id: 'Diesel', text: 'Diesel',
      }, {
        id: 'Gas', text: 'Gas',
      }, {
        id: 'Off Road Diesel', text: 'Off Road Diesel',
      }],
      defaultSelectFuelTypeVal: 'Gas',
      selectVendorData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname Lastname',
      }, {
        id: '003', text: 'Firstname Lastname',
      }],
      defaultSelectVendorVal: '001',
      selectDriverData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectDriverVal: '002',
      selectCostcenterData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectCostcenterVal: '003',
      selectStocktankData: [{
        id: '001', text: 'Firstname Lastname',
      }, {
        id: '002', text: 'Firstname2 Lastname2',
      }, {
        id: '003', text: 'Firstname3 Lastname3',
      }],
      defaultSelectStocktankVal: '003',
      selectStocktankData: [{
        id: '001', text: 'Tank 1',
      }, {
        id: '002', text: 'Tank 2',
      }, {
        id: '003', text: 'Tank 3',
      }],
      defaultSelectStocktankVal: '001',
      selectBrandData: [{
        id: '001', text: 'Petron',
      }, {
        id: '002', text: 'Shell',
      }, {
        id: '003', text: 'PTT',
      }],
      defaultSelectBrandVal: '001',
      selectLocationData: [{
        id: '001', text: 'Lawaan',
      }, {
        id: '002', text: 'Dulju',
      }, {
        id: '003', text: 'Pit-os',
      }],
      defaultSelectLocationVal: '001',
      selectLocationData: [{
        id: '001', text: 'Lawaan',
      }, {
        id: '002', text: 'Dulju',
      }, {
        id: '003', text: 'Pit-os',
      }],
      defaultSelectLocationVal: '001',
    };
  }
  /* eslint-disable */
  createCustomSearchField (props) {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  toggleTwo () {
    this.setState({
      dropdownOpenTwo: !this.state.dropdownOpenTwo,
    });
  };

  /* eslint-enable */
  renderSizePerPageDropDown (props) {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

   toggleFirstTabs(tab) {
    if (this.state.activeFirstTab !== tab) {
      this.setState({
        activeFirstTab: tab,
      });
    }
  }

  render() {
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
    };

    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/all">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>history</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">History</span></h2>
        
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
              className="btn btn-inverse btn-lg"
              to="/app/fleet/all"
              >
              <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            <h3 className="text-right">Q076-D5B Cat Dozer</h3>
            <h4 className="text-right">Hours 6,833 - Mileage: 3,345</h4>
          </Col>
        </Row>
        <Widget>
          <Nav  tabs className="tabs-right flex-column">
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'maintenanceTab' })}
                onClick={() => {
                  this.toggleFirstTabs('maintenanceTab');
                }}
              >
                <span>Maintenace</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'accidentsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('accidentsTab');
                }}
              >
                <span>Accidents</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'assignmentsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('assignmentsTab');
                }}
              >
                <span>Assignments</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'fluidsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('fluidsTab');
                }}
              >
                <span>Fluids</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'fuelTab' })}
                onClick={() => {
                  this.toggleFirstTabs('fuelTab');
                }}
              >
                <span>Fuel</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'inspectionsTab' })}
                onClick={() => {
                  this.toggleFirstTabs('inspectionsTab');
                }}
              >
                <span>Inspections</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'tireTab' })}
                onClick={() => {
                  this.toggleFirstTabs('tireTab');
                }}
              >
                <span>Tire History</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeFirstTab === 'generalexpenseTab' })}
                onClick={() => {
                  this.toggleFirstTabs('generalexpenseTab');
                }}
              >
                <span>General Expense</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                  className={classnames({ active: this.state.activeFirstTab === 'meterlogTab' })}
                  onClick={() => {
                    this.toggleFirstTabs('meterlogTab');
                  }}
                >
                <span>Meter Log</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                  className={classnames({ active: this.state.activeFirstTab === 'meterreplacementTab' })}
                  onClick={() => {
                    this.toggleFirstTabs('meterreplacementTab');
                  }}
                >
                <span>Meter Replacement</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                  className={classnames({ active: this.state.activeFirstTab === 'picturesTab' })}
                  onClick={() => {
                    this.toggleFirstTabs('picturesTab');
                  }}
                >
                <span>Pictures</span>
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeFirstTab}>
            <TabPane tabId="maintenanceTab" >
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="accidentsTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="assignmentsTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="fluidsTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="fuelTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="inspectionsTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="inspectionsTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="tireTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="generalexpenseTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="meterlogTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="meterreplacementTab">
              <Row>
                <Col md="8">
                  <ButtonToolbar className="float-left">
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-print" />
                    </Button>
                    <Button color="inverse" className="mr-xs">
                        <i className="fa fa-gear" />
                    </Button>
                    <Button color="primary"><i className="fa fa-plus" /> New</Button>
                    <Button color="primary" disabled><i className="fa fa-edit" /> Edit</Button>
                    <Button color="primary" disabled><i className="fa fa-times" /> Delete</Button>
                  </ButtonToolbar>
                </Col>
                <Col md="4" className="text-left">
                  
                </Col>
              </Row>
              <BootstrapTable data={this.state.reactBootstrapTable} version="4" pagination options={options} search>
                <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="info" dataFormat={infoFormatter}>Info</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="description">Description</TableHeaderColumn>
                <TableHeaderColumn className="d-none d-md-table-cell" columnClassName="d-none d-md-table-cell" dataField="date" dataSort sortFunc={dateSortFunc}>Date</TableHeaderColumn>
                <TableHeaderColumn className="width-150" columnClassName="width-150" dataField="status" dataSort dataFormat={progressFormatter} sortFunc={progressSortFunc}>Status</TableHeaderColumn>
              </BootstrapTable>
            </TabPane>
            <TabPane tabId="picturesTab">
              testing
            </TabPane>
          </TabContent>
        </Widget>
      </div>
    );
  }

}

// export default withStyles(s)(Fleethistory);

function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

// export default withStyles(s)(Fleetinspect);
export default withRouter(connect(mapStateToProps)(withStyles(s)(Fleethistory)));