import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import PropTypes  from 'prop-types';
import classnames from 'classnames';

import Datetime from 'react-datetime';
import { LinkRouter, browserHistory } from 'react-router'; 
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';
import Dropzone from 'react-dropzone';
import TextareaAutosize from 'react-autosize-textarea';

import { reactTableData, reactBootstrapTableData } from './data';

import Widget from '../../../components/Widget';
import s from './Fleetrepair.scss';
import { fetchrepairparts, checkboxrepair } from "../../../actions/repairvehicle";
import { checkboxfleet } from "../../../actions/fleet";

class Fleetrepair extends React.Component {

  constructor(props) {
    super(props);

    this.toggleFirstTabs = this.toggleFirstTabs.bind(this);
    this.toggleSecondTabs = this.toggleSecondTabs.bind(this);
    this.toggleThirdTabs = this.toggleThirdTabs.bind(this);
    this.toggleFourthTabs = this.toggleFourthTabs.bind(this);
    this.toggleFifthTabs = this.toggleFifthTabs.bind(this);

    this.state = {
      isDatePickerOpen: false,
      dropdownOpenTwo: false,
      activeFirstTab: 'maintenanceTab',
      activeSecondTab: 'partsTab',
      activeThirdTab: 'tab33',
      activeFourthTab: 'tab43',
      activeFifthTab: 'tab52',
      dropdownOpen: false,
      reactTable: reactTableData(),
      reactBootstrapTable: reactBootstrapTableData(),
      selectAssignedData: [{
        id: '1',
        text: 'EMPLOYEE',
        children: [{
          id: '11', text: 'Dallas Cowboys',
        }, {
          id: '12', text: 'New York Giants',
        }, {
          id: '13', text: 'Philadelphia Eagles',
        }, {
          id: '14', text: 'Washington Redskins',
        }],
      }, {
        id: '2',
        text: 'VENDOR',
        children: [{
          id: '21', text: 'Chicago Bears',
        }, {
          id: '22', text: 'Detroit Lions',
        }, {
          id: '23', text: 'Green Bay Packers',
        }, {
          id: '24', text: 'Minnesota Vikings',
        }],
      }],
      selectPriorityData: [{
        id: 'Normal', text: 'Normal',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultSelectPriorityVal: 'Normal',
      selectTypeData: [{
        id: 'Normal', text: 'select type data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectTypeVal: 'Normal',
      selectCostcenterData: [{
        id: 'Normal', text: 'Cost Center Data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectCostcenterVal: 'Normal',
      selectInvoiceData: [{
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }],
      defaultselectInvoiceVal: 'Normal',
      selectMechanic: [{
        id: '123456789', text: 'Driver 1',
      }, {
        id: 'driver2', text: 'Driver 2',
      }, {
        id: 'driver3', text: 'Driver 3',
      }, {
        id: 'driver4', text: 'Driver 4',
      }],
      defaultSelectMechanic: '123456789',
      dropFiles: [],
      toggleAddRepair: false,
      readyEditButton: false,
    };

    this.onDrop = this.onDrop.bind(this);
    this.toggleAddRepair = this.toggleAddRepair.bind(this);
  }
  componentWillMount = () => {
    this.props.fetchrepairparts();
  }

  onDrop(files) {
    this.setState({
      dropFiles: files,
    });
  }

  onRowSelect = (row, isSelected, e) => {
    // if (isSelected) {
    //   this.setState({
    //     readyEditButton: true,
    //   });
    // } else {
    //   this.setState({
    //     readyEditButton: false,
    //   });
    // }

    let checkboxlistNew = [];
    if (isSelected) {
      if (Object.values(this.props.checkboxlist.checkboxrepair).length) {
        checkboxlistNew = Object.values(this.props.checkboxlist.checkboxfleet);
        // console.log(checkboxlistNew);
        // console.log(row);
        checkboxlistNew.push(row);
      } else {
        checkboxlistNew.push(row);
      }
      this.setState({
        readyEditButton: true,
      });
    } else {
      const checkboxlistOld = Object.values(this.props.checkboxlist.checkboxfleet);
      
      for (var i=0; i < checkboxlistOld.length; i++) {
          if (checkboxlistOld[i].id != row.id) {
            checkboxlistNew.push(checkboxlistOld[i]);
          }
      }
      
    }
    if (checkboxlistNew) {
      this.setState({
        readyEditButton: false,
      });
    }
    this.props.checkboxrepair(checkboxlistNew);
  }

  onSelectAll = (isSelected, rows) => {
    if (isSelected) {
      this.setState({
        readyEditButton: true,
      });
    } else {
      this.setState({
        readyEditButton: false,
      });
    }
  }

  toggleAddRepair() {
    this.setState({
      toggleAddRepair: !this.state.toggleAddRepair,
    });
  }

  toggleAddDelete() {
    // this.setState({

    // });
  }

  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  toggleTwo = () => {
    this.setState({
      dropdownOpenTwo: !this.state.dropdownOpenTwo,
    });
  };

  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  toggleFirstTabs = (tab) => {
    if (this.state.activeFirstTab !== tab) {
      this.setState({
        activeFirstTab: tab,
      });
    }
  };

  toggleSecondTabs = (tab) => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab,
      });
    }
  };

  toggleThirdTabs = (tab) => {
    if (this.state.activeThirdTab !== tab) {
      this.setState({
        activeThirdTab: tab,
      });
    }
  };

  toggleFourthTabs = (tab) => {
    if (this.state.activeFourthTab !== tab) {
      this.setState({
        activeFourthTab: tab,
      });
    }
  };

  toggleFifthTabs = (tab) => {
    if (this.state.activeFifthTab !== tab) {
      this.setState({
        activeFifthTab: tab,
      });
    }
  };

  render() {
    let repairvehicle;
    let repairPartsVehicle = [];
    let repairMechanic = [];

    if (this.props.repairvehicle) {
      repairvehicle = this.props.repairvehicle;
      console.log(this.props.repairvehicle);
      this.props.repairvehicle.map(rVehicle => {
        if (Object.values(rVehicle.partsVehicle).length) {
          // rVehicle.partsVehicle.forEach( (pVList) => {
            console.log('some sulod');
            console.log(rVehicle.partsVehicle);
            repairPartsVehicle.push(rVehicle.partsVehicle);
          // });
          // return rVehicle.partsVehicle;
        }

        if (Object.values(rVehicle.mechanic).length) {
          repairMechanic.push(rVehicle.mechanic);
        }
        
       
      });
      console.log(repairPartsVehicle);
    }

    const options = {
      sizePerPage: 10,
      noDataText: 'Loading data',
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField
    };

    const selectRow = {
      mode: 'checkbox',
      bgColor: 'rgba(249, 249, 249,.8)',
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll,
    };


    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/all">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Repair</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Repair</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
              className="btn btn-inverse btn-lg"
              to="/app/fleet/all"
              >
              <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            &nbsp;
          </Col>
          <Col lg={4}  className="float-center">
            &nbsp;
          </Col>
          <Col lg={4} className="float-right">
            &nbsp;
          </Col>
        </Row>
        <Row>
          <Col lg={8}>
            <Widget title={<h4>Equipment or Tasks</h4>} refresh close>
              <Row>
                &nbsp;
              </Row>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'maintenanceTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('maintenanceTab');
                    }}
                  >
                    <span>Maintenace</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'partsTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('partsTab');
                    }}
                  >
                    <span>Parts</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'laborTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('laborTab');
                    }}
                  >
                    <span>Labor</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'attachmentsTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('attachmentsTab');
                    }}
                  >
                    <span>Attachments</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'notesTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('notesTab');
                    }}
                  >
                    <span>Notes</span>
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeFirstTab}>
                <TabPane tabId="maintenanceTab">
                <BootstrapTable 
                  data={repairvehicle}
                  version="4" 
                  pagination
                  options={options}
                  selectRow={selectRow}
                  striped
                  search >
                  <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" hidden isKey>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField="repairName">Repair Name</TableHeaderColumn>
                </BootstrapTable>

                  <ButtonToolbar className="float-left">
                    <Button color="inverse" onClick={this.toggleAddRepair}>Add Repair</Button>
                    <Button color="primary" disabled={!this.state.readyEditButton} onClick={this.toggleAddRepair}>Edit</Button>

                    {/* <Button color="primary" disabled>Edit</Button> */}
                    <Button color="primary" disabled={!this.state.readyEditButton} onClick={this.toggleDeleteRepair}>Delete</Button>
                    {/* <Button color="primary">Quick Select</Button> */}
                  </ButtonToolbar>
                  {/* para gawas ang modal */}
                  <Modal isOpen={this.state.toggleAddRepair} toggle={this.toggleAddRepair}>
                    <FormGroup>
                      <Form  onSubmit={this.onSubmit}>
                        <ModalHeader toggle={this.toggleAddRepair}>Add Repair information</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                              <Label for="vehicle" md={4} className="text-md-right">
                                Repair Name:
                              </Label>
                              <Col md={7}>
                                <Input  onChange={this.onChange} type="text" name="repairName" id="repairName" placeholder="Repair Name" value={this.state.repairName}/>
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Label for="vehicle" md={4} className="text-md-right">
                                Mechanic:
                              </Label>
                              <Col md={7}>
                                {/* <Input  onChange={this.onChange} type="text" name="mechanic" id="mechanic" placeholder="Mechanic" value={this.state.mechanic}/> */}
                                <Select2
                                  name="mechanic"
                                  id="mechanic"
                                  className={s.select2}
                                  value={this.state.defaultSelectMechanic}
                                  data={this.state.selectMechanic}
                                  block
                                />
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Col md={7}>
                                <Input hidden   type="text" name="vehicle" id="vehicle" placeholder="Vehicle" value={this.state.repairName}/>
                              </Col>
                            </FormGroup> 
                        </ModalBody>
                        <ModalFooter>
                          <Button color="secondary" onClick={this.toggleAddRepair}>Cancel</Button>
                          <Button color="primary">Save changes</Button>
                        </ModalFooter>
                      </Form>
                    </FormGroup>
                  </Modal>
                  {/* end para gawas ang modal sa add repair */}
                </TabPane>
                <TabPane tabId="partsTab">
                <BootstrapTable 
                  data={repairPartsVehicle}
                  version="4" 
                  pagination
                  options={options}
                  selectRow={selectRow}
                  striped
                  search >
                  <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" hidden isKey>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField="dateEstimatedExpiration">Estimated date</TableHeaderColumn>
                  <TableHeaderColumn dataField="estimatedDistanceLimit">Estimated distance</TableHeaderColumn>
                  <TableHeaderColumn dataField="part">Part</TableHeaderColumn>
                </BootstrapTable>
                  <ButtonToolbar className="float-left">
                    <Button color="inverse">Add a Part</Button>
                    <Button color="primary" disabled>Edit</Button>
                    <Button color="primary" disabled>Delete</Button>
                    {/* <Button color="primary">Quick Select</Button> */}
                  </ButtonToolbar>
                </TabPane>
                <TabPane tabId="laborTab">
                  <BootstrapTable 
                    data={repairMechanic}
                    version="4" 
                    pagination
                    options={options}
                    selectRow={selectRow}
                    striped
                    search >
                    <TableHeaderColumn className="width-50" columnClassName="width-50" dataField="id" hidden isKey>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField="firstname">First name</TableHeaderColumn>
                    <TableHeaderColumn dataField="middlename">Middle name</TableHeaderColumn>
                    <TableHeaderColumn dataField="lastname">Last name</TableHeaderColumn>
                  </BootstrapTable>
                  <ButtonToolbar className="float-left">
                    <Button color="inverse">Add a Part</Button>
                    <Button color="primary" disabled>Edit</Button>
                    <Button color="primary" disabled>Delete</Button>
                    {/* <Button color="primary">Quick Select</Button> */}
                  </ButtonToolbar>
                </TabPane>
                <TabPane tabId="attachmentsTab">
                  <div>
                    <Dropzone
                      onDrop={this.onDrop} accept="image/*"
                      className={`${s.dropzone} dropzone`}
                    >
                      {this.state.dropFiles.length > 0 ? <div>
                        {this.state.dropFiles.map((file, idx) => (
                          <div className="display-inline-block mr-xs mb-xs" key={`drop-id-${idx.toString()}`}>
                            <img alt="..." src={file.preview} width={100} />
                            <div>{file.name}</div>
                          </div>
                        ))}
                      </div> : <p>This dropzone accepts only images.
                        Try dropping some here, or click to select files to upload.</p>}
                    </Dropzone>

                  </div>
                </TabPane>
                <TabPane tabId="notesTab">
                <FormGroup row> {/* todo: fix autosize */}
                  
                    <TextareaAutosize
                      rows={3} id="elastic-textarea"
                      placeholder="Try to add few new lines.."
                      className={`form-control ${s.autogrow} transition-height input-transparent`}
                    />
                  
                </FormGroup>
                </TabPane>
              </TabContent>
            </Widget>
            
          </Col>
          <Col lg={4} className="float-right text-md-left">
            <Row>
              <Col lg={6}>
                <Button color="inverse" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save and Next</Button>
              </Col>
              <Col lg={6}>
                <Button color="primary" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save</Button>
              </Col>
            </Row>
            <Widget  refresh close>
              <FormGroup row>
                <Label md="4" for="selectEquipment">Equipment:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRequested">Requested:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Repair by:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup  row>
              <Label md="4" for="dateScheduled">Due date:&nbsp;</Label>
              <Col md="7">
                <Datetime 
                  id="dateScheduled"
                  open={this.state.isDatePickerOpen}
                  viewMode="days" timeFormat={false}
                  inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
                />
                <InputGroupAddon md="4" onClick={() => { this.refDatePicker.focus(); }}>
                  <i className="glyphicon glyphicon-th" />
                </InputGroupAddon>
              </Col>
              </FormGroup>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }

}

Fleetrepair.propTypes = {
  fetchrepairparts: PropTypes.func.isRequired,
  checkboxfleet: PropTypes.func.isRequired,
  checkboxrepair: PropTypes.func.isRequired
}

// export default withStyles(s)(Fleetrepair);
function mapStateToProps(store) {
  return {
    repairvehicle: store.repairvehicle.repairvehicle,
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
    checkboxlist: store.checkboxlist,
    checkboxfleet: PropTypes.func.isRequired,
    checkboxRepairlist: store.checkboxRepairlist,
    checkboxrepair: PropTypes.func.isRequired
  };
}

export default withRouter(connect(mapStateToProps, {fetchrepairparts, checkboxfleet, checkboxrepair})(withStyles(s)(Fleetrepair)));
// export default withRouter(connect(mapStateToProps, {fetchFleets, fetchFleetParts, checkboxfleet})(styledComponent));