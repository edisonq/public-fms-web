import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Router, browserHistory } from 'react-router'; 
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';

import { reactTableData, reactBootstrapTableData } from './data';

import Widget from '../../../components/Widget';
import s from './Fleetinspect.scss';

class Fleetinspect extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isDatePickerOpen: false,
      dropdownOpenTwo: false,
      dropdownOpen: false,
      reactTable: reactTableData(),
      reactBootstrapTable: reactBootstrapTableData(),
      selectAssignedData: [{
        id: '1',
        text: 'EMPLOYEE',
        children: [{
          id: '11', text: 'Dallas Cowboys',
        }, {
          id: '12', text: 'New York Giants',
        }, {
          id: '13', text: 'Philadelphia Eagles',
        }, {
          id: '14', text: 'Washington Redskins',
        }],
      }, {
        id: '2',
        text: 'VENDOR',
        children: [{
          id: '21', text: 'Chicago Bears',
        }, {
          id: '22', text: 'Detroit Lions',
        }, {
          id: '23', text: 'Green Bay Packers',
        }, {
          id: '24', text: 'Minnesota Vikings',
        }],
      }],
      selectPriorityData: [{
        id: 'Normal', text: 'Normal',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultSelectPriorityVal: 'Normal',
      selectTypeData: [{
        id: 'Normal', text: 'select type data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectTypeVal: 'Normal',
      selectCostcenterData: [{
        id: 'Normal', text: 'Cost Center Data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectCostcenterVal: 'Normal',
      selectInvoiceData: [{
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }],
      defaultselectInvoiceVal: 'Normal',
    };
  }
  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  toggleTwo = () => {
    this.setState({
      dropdownOpenTwo: !this.state.dropdownOpenTwo,
    });
  };

  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  

  render() {
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
    };

    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><Link to="/app/fleet/all">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>Inspection</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">FLEET - <span className="fw-semi-bold">Inspection</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link
              className="btn btn-inverse btn-lg"
              to="/app/fleet/all"
              >
              <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            &nbsp;
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            &nbsp;
          </Col>
          <Col lg={4}  className="float-center">
            &nbsp;
          </Col>
          <Col lg={4} className="float-right">
            &nbsp;
          </Col>
        </Row>
        <Row>
          <Col lg={8}>
            <Widget title={<h4>Inspection Items</h4>} refresh close>
              <Row>
                &nbsp;
              </Row>
              
                    <ReactTable
                        data={this.state.reactTable}
                        filterable
                        columns={[
                          {
                            Header: 'NAME',
                            accessor: 'name',
                          },
                          {
                            Header: 'POSITION',
                            accessor: 'position',
                          },
                          {
                            Header: 'OFFICE',
                            accessor: 'office',
                          },
                          {
                            Header: 'EXT',
                            accessor: 'ext',
                          },
                          {
                            Header: 'START DATE',
                            accessor: 'startDate',
                          },
                          {
                            Header: 'SALARY',
                            accessor: 'salary',
                          },
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                      />

                  <ButtonToolbar className="float-left">
                    <Button color="primary" disabled>Edit</Button>
                    <Button color="primary" disabled>Delete</Button>
                    {/* <Button color="primary">Quick Select</Button> */}
                  </ButtonToolbar>
                
            </Widget>
            
          </Col>
          <Col lg={4} className="float-right text-md-left">
            <Row>
              <Col lg={6}>
                <Button color="inverse" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save and Next</Button>
              </Col>
              <Col lg={6}>
                <Button color="primary" className="mb-xs mr-xs" block><i className="fa fa-floppy-o" /> Save</Button>
              </Col>
            </Row>
            <Widget  refresh close>
              <FormGroup row>
                <Label md="4" for="selectEquipment">Equipment:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRequested">Inspector:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Mileage:</Label>
                <Col md="7">
                  <Input type="text" id="normal-field" placeholder="May have placeholder" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectRepairby">Condition:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup  row>
              <Label md="4" for="dateScheduled">Due date:&nbsp;</Label>
              <Col md="7">
                <Datetime 
                  id="dateScheduled"
                  open={this.state.isDatePickerOpen}
                  viewMode="days" timeFormat={false}
                  inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
                />
                <InputGroupAddon md="4" onClick={() => { this.refDatePicker.focus(); }}>
                  <i className="glyphicon glyphicon-th" />
                </InputGroupAddon>
              </Col>
              </FormGroup>
              <a
                className="btn btn-inverse  btn-block"
                href="/app/issueworkorders"
              >
              <i className="fa fa-wrench" />&nbsp;&nbsp;Request Repair
              </a>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }

}
function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

// export default withStyles(s)(Fleetinspect);
export default withRouter(connect(mapStateToProps)(withStyles(s)(Fleetinspect)));