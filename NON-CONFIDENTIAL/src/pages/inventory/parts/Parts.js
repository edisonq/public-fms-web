import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Breadcrumb,
  BreadcrumbItem,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import PropTypes  from 'prop-types';
import { connect } from 'react-redux';

import ReactTable from 'react-table';

import { reactTableData, reactBootstrapTableData } from './data';
import Widget from '../../../components/Widget';
import { fetchParts } from "../../../actions/parts";
import Parttop from '../../../components/Parttop/Parttop';
import s from './Parts.scss';


class Parts extends React.Component {

  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     reactTable: reactTableData(),
  //     reactBootstrapTable: reactBootstrapTableData(),
  //   };
  // }

  componentWillMount = () => {
    this.props.fetchParts();
  }

  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };
  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  render() {
    let partItems = this.props.parts;

    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
    };

    if (this.props.parts) {
      partItems =  this.props.parts.map(part => {
        return part;
      });
    } 

    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem><a href="/app/fleet/overdue">Inventory</a></BreadcrumbItem>
          <BreadcrumbItem active>Parts</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">Tables - <span className="fw-semi-bold">Dynamic</span></h2>
        <Widget title={<h4>React <span className="fw-semi-bold">Table</span></h4>} refresh>
          <Parttop />
          <ReactTable
            data={partItems}
            filterable
            resizable

            columns={[
              {
                Header: 'Manufacturer Serial Number',
                accessor: 'manufacturerserialnumber',
              },
              {
                Header: 'limitMileage',
                accessor: 'limitMileage',
              }
            ]}
            defaultPageSize={10}
            className="-striped -highlight"
          />
        </Widget>

      </div>
    );
  }

}

// export default withStyles(s)(Parts);
Parts.propTypes = {
  fetchParts: PropTypes.func.isRequired,
  // parts: PropTypes.array.isRequired,
  // newPost: PropTypes.object
}

const mapStateToProps = state => ({
  parts: state.parts.items,
  fetchParts: PropTypes.func.isRequired,
  reactTable: reactTableData(),
  reactBootstrapTable: reactBootstrapTableData(),
  // newPost: state.posts.item
});

const styledComponent = withStyles(s)(Parts);
export default connect(mapStateToProps, {fetchParts})(styledComponent);
// export default withStyles(s)(Overdue);
