import React from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonDropdown,
  ButtonToolbar,
  Breadcrumb,
  BreadcrumbItem,
  Col,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledNavDropdown,
} from 'reactstrap';

import {
  BootstrapTable,
  TableHeaderColumn,
  SearchField,
} from 'react-bootstrap-table';

import classnames from 'classnames';

import Datetime from 'react-datetime';
import { Link, withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import MaskedInput from 'react-maskedinput';
import Select2 from 'react-select2-wrapper';

import { reactTableData, reactBootstrapTableData } from './data';

import Widget from '../../../components/Widget';
import s from './Issueworkorder.scss';

class Issueworkorders extends React.Component {

  constructor(props) {
    super(props);

    this.toggleFirstTabs = this.toggleFirstTabs.bind(this);
    this.toggleSecondTabs = this.toggleSecondTabs.bind(this);
    this.toggleThirdTabs = this.toggleThirdTabs.bind(this);
    this.toggleFourthTabs = this.toggleFourthTabs.bind(this);
    this.toggleFifthTabs = this.toggleFifthTabs.bind(this);

    this.state = {
      isDatePickerOpen: false,
      dropdownOpenTwo: false,
      activeFirstTab: 'maintenanceTab',
      activeSecondTab: 'partsTab',
      activeThirdTab: 'tab33',
      activeFourthTab: 'tab43',
      activeFifthTab: 'tab52',
      dropdownOpen: false,
      reactTable: reactTableData(),
      reactBootstrapTable: reactBootstrapTableData(),
      selectAssignedData: [{
        id: '1',
        text: 'EMPLOYEE',
        children: [{
          id: '11', text: 'Dallas Cowboys',
        }, {
          id: '12', text: 'New York Giants',
        }, {
          id: '13', text: 'Philadelphia Eagles',
        }, {
          id: '14', text: 'Washington Redskins',
        }],
      }, {
        id: '2',
        text: 'VENDOR',
        children: [{
          id: '21', text: 'Chicago Bears',
        }, {
          id: '22', text: 'Detroit Lions',
        }, {
          id: '23', text: 'Green Bay Packers',
        }, {
          id: '24', text: 'Minnesota Vikings',
        }],
      }],
      selectPriorityData: [{
        id: 'Normal', text: 'Normal',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultSelectPriorityVal: 'Normal',
      selectTypeData: [{
        id: 'Normal', text: 'select type data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectTypeVal: 'Normal',
      selectCostcenterData: [{
        id: 'Normal', text: 'Cost Center Data',
      }, {
        id: 'Medium', text: 'Medium',
      }, {
        id: 'High', text: 'High',
      }, {
        id: 'Urgent', text: 'Urgent',
      }],
      defaultselectCostcenterVal: 'Normal',
      selectInvoiceData: [{
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }, {
        id: '123456789', text: '123456789',
      }],
      defaultselectInvoiceVal: 'Normal',
    };
  }
  /* eslint-disable */
  createCustomSearchField = (props) => {
    return (
      <SearchField
        className="input-transparent"
        placeholder="Search"
      />
    );
  };

  toggleTwo = () => {
    this.setState({
      dropdownOpenTwo: !this.state.dropdownOpenTwo,
    });
  };

  /* eslint-enable */
  renderSizePerPageDropDown = (props) => {
    const limits = [];
    props.sizePerPageList.forEach((limit) => {
      limits.push(
        <DropdownItem
          key={limit}
          onClick={() => props.changeSizePerPage(limit)}
        >
          { limit }
        </DropdownItem>,
      );
    });

    return (
      <Dropdown isOpen={props.open} toggle={props.toggleDropDown}>
        <DropdownToggle caret>
          { props.currSizePerPage }
        </DropdownToggle>
        <DropdownMenu>
          { limits }
        </DropdownMenu>
      </Dropdown>
    );
  };

  toggleFirstTabs = (tab) => {
    if (this.state.activeFirstTab !== tab) {
      this.setState({
        activeFirstTab: tab,
      });
    }
  };

  toggleSecondTabs = (tab) => {
    if (this.state.activeSecondTab !== tab) {
      this.setState({
        activeSecondTab: tab,
      });
    }
  };

  toggleThirdTabs = (tab) => {
    if (this.state.activeThirdTab !== tab) {
      this.setState({
        activeThirdTab: tab,
      });
    }
  };

  toggleFourthTabs = (tab) => {
    if (this.state.activeFourthTab !== tab) {
      this.setState({
        activeFourthTab: tab,
      });
    }
  };

  toggleFifthTabs = (tab) => {
    if (this.state.activeFifthTab !== tab) {
      this.setState({
        activeFifthTab: tab,
      });
    }
  };

  render() {
    const options = {
      sizePerPage: 10,
      paginationSize: 3,
      sizePerPageDropDown: this.renderSizePerPageDropDown,
      searchField: this.createCustomSearchField,
    };

    function infoFormatter(cell) {
      return (
        <div>
          <small>
            <span className="fw-semi-bold">Type:</span>&nbsp;{cell.type}
          </small>
          <br />
          <small>
            <span className="fw-semi-bold">Dimensions:</span>&nbsp;{cell.dimensions}
          </small>
        </div>
      );
    }

    function progressFormatter(cell) {
      return (
        <Progress color={cell.type} value={cell.progress} />
      );
    }

    function progressSortFunc(a, b, order) {
      if (order === 'asc') {
        return a.status.progress - b.status.progress;
      }
      return b.status.progress - a.status.progress;
    }

    function dateSortFunc(a, b, order) {
      if (order === 'asc') {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
      }
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem ><Link to="/app/fleet/all">FLEET</Link></BreadcrumbItem>
          <BreadcrumbItem active>New</BreadcrumbItem>
        </Breadcrumb>
        <h2 className="page-title">Issue - <span className="fw-semi-bold">Work order</span></h2>
        <Row >
          <Col lg={4} md={4} sm={4}>
            <Link to="/app/fleet/all" className="btn btn-inverse btn-lg"  exact>
                <i className="glyphicon glyphicon-arrow-left" /> CANCEL
            </Link>
            {/* <Button color="inverse" size="lg" onClick={browserHistory.goBack} >CANCEL</Button> */}
          </Col>
          <Col lg={8} md={8} sm={8} className="option-div">
            <Button color="primary" size="lg" className="mb-xs mr-xs">Open</Button>
            <Button color="inverse" size="lg" className="mb-xs mr-xs">In-Progress</Button>
            <Button color="inverse" size="lg" className="mb-xs mr-xs">On-Hold</Button>
            <Button color="inverse" size="lg" className="mb-xs mr-xs">Complete</Button>
            {/* <a className="btn btn-primary btn-lg" href="/app/fleet/repairs">Open</a>
            <a className="btn btn-inverse btn-lg" href="/app/fleet/repairs">In-Progress</a>
            <a className="btn btn-inverse btn-lg" href="/app/fleet/repairs">On-Hold</a>
            <a className="btn btn-inverse btn-lg" href="/app/fleet/repairs">Complete</a> */}
            <div className="datepicker">
                <Datetime
                  id="datepicker"
                  open={this.state.isDatePickerOpen}
                  viewMode="days" timeFormat={false}
                  inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
                />
                <InputGroupAddon onClick={() => { this.refDatePicker.focus(); }}>
                  <i className="glyphicon glyphicon-th" />
                </InputGroupAddon>
              </div>
          </Col>
        </Row>
        <Row className="rowDateArea">
          <Col lg={4}>
            <div className="datepicker">
            <Label for="dateDue">Due:&nbsp;</Label>
              <Datetime
                id="dateDue"
                open={this.state.isDatePickerOpen}
                viewMode="days" timeFormat={false}
                inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
              />
              <InputGroupAddon onClick={() => { this.refDatePicker.focus(); }}>
                <i className="glyphicon glyphicon-th" />
              </InputGroupAddon>
            </div>
          </Col>
          <Col lg={4}  className="float-center">
            <div className="datepicker" >
            <Label for="dateStarted">Started:&nbsp;</Label>
              <Datetime
                id="dateStarted"
                open={this.state.isDatePickerOpen}
                viewMode="days" timeFormat={false}
                inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
              />
              <InputGroupAddon onClick={() => { this.refDatePicker.focus(); }}>
                <i className="glyphicon glyphicon-th" />
              </InputGroupAddon>
            </div>
          </Col>
          <Col lg={4} className="float-right">
            <div className="datepicker" >
            <Label for="dateScheduled">Scheduled:&nbsp;</Label>
              <Datetime
                id="dateScheduled"
                open={this.state.isDatePickerOpen}
                viewMode="days" timeFormat={false}
                inputProps={{ ref: (input) => { this.refDatePicker = input; } }}
              />
              <InputGroupAddon onClick={() => { this.refDatePicker.focus(); }}>
                <i className="glyphicon glyphicon-th" />
              </InputGroupAddon>
            </div>
          </Col>
        </Row>
        <Row>
          <Col lg={8}>
            <Widget title={<h4>Equipment or Tasks</h4>} refresh close>
              <Row>
                <Col lg={6}>
                  <FormGroup>
                  <InputGroup>
                    <Input type="search" id="search-input" value="Default value" />
                    <InputGroupButton><Button
                      color="secondary"
                    >Search</Button></InputGroupButton>
                  </InputGroup>
                  </FormGroup>
                </Col>
                <Col lg={6}>
                  <InputGroup>
                    <InputGroupAddon><i className="fa fa-tachometer" /></InputGroupAddon>
                    <Input id="prepended-input" className="input-transparent" type="test" size="16" placeholder="Username" />
                  </InputGroup>
                </Col>
              </Row>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'maintenanceTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('maintenanceTab');
                    }}
                  >
                    <span>Maintenace</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'partsTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('partsTab');
                    }}
                  >
                    <span>Parts</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'laborTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('laborTab');
                    }}
                  >
                    <span>Labor</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'attachmentsTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('attachmentsTab');
                    }}
                  >
                    <span>Attachments</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeFirstTab === 'notesTab' })}
                    onClick={() => {
                      this.toggleFirstTabs('notesTab');
                    }}
                  >
                    <span>Notes</span>
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeFirstTab}>
                <TabPane tabId="maintenanceTab">
                    <ReactTable
                        data={this.state.reactTable}
                        filterable
                        columns={[
                          {
                            Header: 'NAME',
                            accessor: 'name',
                          },
                          {
                            Header: 'POSITION',
                            accessor: 'position',
                          },
                          {
                            Header: 'OFFICE',
                            accessor: 'office',
                          },
                          {
                            Header: 'EXT',
                            accessor: 'ext',
                          },
                          {
                            Header: 'START DATE',
                            accessor: 'startDate',
                          },
                          {
                            Header: 'SALARY',
                            accessor: 'salary',
                          },
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                      />

                  <ButtonToolbar className="float-left">
                    <Button color="inverse">Add</Button>
                    <Button color="primary" disabled>Edit</Button>
                    <Button color="primary" disabled>Delete</Button>
                    {/* <Button color="primary">Quick Select</Button> */}
                  </ButtonToolbar>
                </TabPane>
                <TabPane tabId="partsTab">
                  <div className={s.tabPicture}>
                    <i className="fa fa-picture-o" />
                  </div>
                </TabPane>
                <TabPane tabId="laborTab">
                  <p>The same thing is for startups and ideas. If you have an idea right away
                    after it appears in your mind you should go and make a first step to implement
                    it. If you will think too much it will sink in the swamp of never implemented
                    plans and ideas or will just go away or will be implemented by someone else.
                  </p>
                  <p><strong>5 months of doing everything to achieve nothing.</strong></p>
                  <p>I had an idea named Great Work. It was a service aimed to help people find
                    their passion. Yes I know it sound crazy and super naïve but I worked on
                    that. I started to work on planning, graphics, presentations, pictures,
                    descriptions, articles, investments and so on. I worked on everything
                    but not the project itself
                  </p>
                  <ButtonToolbar>
                    <Button color="danger">Some button</Button>
                    <Button color="default">Cancel</Button>
                  </ButtonToolbar>
                </TabPane>
                <TabPane tabId="attachmentsTab">
                  <p>I realized really simple thing recently. Try is million times better than
                    think. The real world is much more representative than any models either
                    they are built in mind or on the shit of paper.
                  </p>
                  <Button color="warning" block>Some button</Button>
                </TabPane>
                <TabPane tabId="notesTab">
                  <p>sakit akong tiyan
                  </p>
                  <Button color="warning" block>Some button</Button>
                </TabPane>
              </TabContent>
            </Widget>
            <Widget title={<h4>Assigned to</h4>} refresh close>              
              <FormGroup row> 
                <Label md="4" for="selectAssigned">Search and select</Label>
                <Col md="6" className={s.select2}>
                  <Select2
                    data={this.state.selectAssignedData}
                  />
                </Col>
              </FormGroup>
            </Widget>
          </Col>
          <Col lg={4} className="float-right text-md-left">
            <Button color="inverse" size="sm" className="mb-xs mr-xs"><i className="fa fa-envelope" /> Email</Button>
            <Button color="inverse" size="sm" className="mb-xs mr-xs"><i className="fa fa-print" /> Print</Button>
            <ButtonDropdown
                      isOpen={this.state.dropdownOpenTwo}
                      toggle={this.toggleTwo}
                      className="mb-xs mr-xs"
                    >
                      <DropdownToggle caret color="inverse">
                        &nbsp; More &nbsp;
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem>Action</DropdownItem>
                        <DropdownItem>Another Action</DropdownItem>
                        <DropdownItem>Something else here</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>Separated link</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
            <Button color="primary" className="mb-xs mr-xs">Save</Button>
            <Widget title={<h4>Miscellaneous</h4>} refresh close>
              <FormGroup row>
                <Label md="4" for="selectPriority">Priority:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectPriorityVal}
                    data={this.state.selectPriorityData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectType">Type:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultselectTypeVal}
                    data={this.state.selectTypeData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectCostCenter">Cost Center:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectCostcenterVal}
                    data={this.state.selectCostcenterData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="selectPo" md={4} className="text-md-left">
                  PO #:
                  </Label>
                <Col md={7}>
                <Select2
                    className={s.select2}
                    value={this.state.defaultSelectInvoiceVal}
                    data={this.state.selectInvoiceData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label md="4" for="selectInvoice">Invoice #:</Label>
                <Col md="7">
                  <Select2
                    className={s.select2}
                    value={this.state.defaultSelectInvoiceVal}
                    data={this.state.selectInvoiceData}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="selectCustom1" md={4} className="text-md-left">
                  (Custom)
                </Label>
                <Col md={7}>
                  <Input type="text" id="custom1" placeholder="Custom attributes" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="selectCustom2" md={4} className="text-md-left">
                  (Custom)
                </Label>
                <Col md={7}>
                  <Input type="text" id="custom2" placeholder="Custom attributes" />
                </Col>
              </FormGroup>
            </Widget>
            <Widget title={<h4>Totals</h4>} refresh close>
              <FormGroup row>
                <Label for="textSubtotal" md={4} className="text-md-left">
                  Subtotal
                </Label>
                <Col md={7}>
                  <Input type="text" id="textSubtotal" placeholder="0.00" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="textTotalCustom" md={4} className="text-md-left">
                  (Custom)
                </Label>
                <Col md={7}>
                  <Input type="text" id="textTotalCustom" placeholder="0.00" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={4}>
                  <FormGroup className="checkbox abc-checkbox abc-checkbox-info" check>
                    <Input id="checkbox4" type="checkbox" defaultChecked />{' '}
                    <Label for="checkbox4" check>
                      &#37;:
                    </Label>
                  </FormGroup>
                </Col>
                <Col md={7}>
                  <Input type="text" id="textDiscount" placeholder="0.00" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={2} className="textRemovePadding text-md-right">
                  Tax 1&nbsp;
                </Col>
                <Col md={2} className="textRemovePadding">
                  <Input type="text" id="tax1" placeholder="0.00" />  
                </Col>
                <Col md={7}>
                <Input type="text" id="textTax1" placeholder="0.00" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={2} className="textRemovePadding text-md-right">
                  Tax 2&nbsp;
                </Col>
                <Col md={2} className="textRemovePadding">
                  <Input type="text" id="tax2" placeholder="0.00" block />  
                </Col>
                <Col md={7}>
                <Input type="text" id="textTax2" placeholder="0.00" block />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={4}>
                  <FormGroup className="checkbox abc-checkbox abc-checkbox-info" check>
                    <Input id="checkTotal" type="checkbox" defaultChecked />{' '}
                    <Label for="checkTotal" className="totalText" check>
                      Total
                    </Label>
                  </FormGroup>
                </Col>
                <Col md={7}>
                  <Input className="btn-lg" type="text" id="textDiscount" placeholder="0.00" />
                </Col>
              </FormGroup>
              
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }

}
function mapStateToProps(store) {
  return {
    alertsList: store.alerts.alertsList,
    sidebarOpen: store.navigation.sidebarOpen,
    activeItem: store.navigation.activeItem,
  };
}

export default withRouter(connect(mapStateToProps)(withStyles(s)(Issueworkorders)));

// export default withStyles(s)(Issueworkorders);
// export default withRouter(withStyles(s)(Issueworkorders));