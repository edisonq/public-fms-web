import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Row,
  Col,
  Badge,
  Button,
  ButtonDropdown,
  Card,
  Table,
  Nav,
  NavItem,
  NavLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Progress,
} from 'reactstrap';
import $ from 'jquery';
import d3 from 'd3';
import nv from 'nvd3';
import classnames from 'classnames';
import { Sparklines, SparklinesLine } from 'react-sparklines';
import { Scrollbars } from 'react-custom-scrollbars';
// import 'imports-loader?window.jQuery=jquery,this=>window!jquery-ui/ui/widgets/sortable'; //eslint-disable-line

import Widget from '../../components/Widget';
import s from './Dashboard.scss';
import i14 from '../../images/14.png';
import i13 from '../../images/13.png';
import i1 from '../../images/1.png';
import i2 from '../../images/2.png';
import i3 from '../../images/3.png';

const tooltipPlacement = 'bottom';

const sortOptions = {
  connectWith: '.widget-container',
  handle: 'header, .handle',
  cursor: 'move',
  iframeFix: false,
  items: '.widget:not(.locked)',
  opacity: 0.8,
  helper: 'original',
  revert: true,
  forceHelperSize: true,
  placeholder: 'widget widget-placeholder',
  forcePlaceholderSize: true,
  tolerance: 'pointer',
};

class Dashboard extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activeTab: 1,
      messages: [
        {
          id: 1,
          time: '4 min',
          sender: 'Tikhon Laninga',
          text: `Hey Sam, how is it going? But I must explain to you how all this mistaken
      idea of denouncing of a pleasure and praising pain was born`,
          image: i2,
        },
        {
          id: 2,
          time: '3 min',
          sender: 'Cenhelm Houston',
          text: `Pretty good. Doing my homework..  No one rejects, dislikes, or avoids
        pleasure itself, because it is pleasure, but because`,
          image: i1,
        },
        {
          id: 3,
          time: '2 min',
          sender: 'Tikhon Laninga',
          text: `Anys chance to go out? To take a trivial example, which of us ever undertakes
        laborious physical exercise, except to obtain some advantage`,
          image: i2,
        },
        {
          id: 4,
          time: '2 min',
          sender: 'Cenhelm Houston',
          text: `.. Maybe 40-50 mins. I don't know exactly. On the other hand, we denounce
      with righteous indignation and dislike men who are so beguiled`,
          image: i1,
        },
        {
          id: 5,
          time: '1 min',
          sender: 'Tikhon Laninga',
          text: `Anyway sounds great! These cases are perfectly simple and easy to
      distinguish.`,
          image: i2,
        },
      ],
      newMessage: '',
      dropdownQuickAction: false,
    };

    this.tableSparklineValues = [[], [], [], [], []];
    this.toggleActiveTab = this.toggleActiveTab.bind(this);
    this.messageInputUpdate = this.messageInputUpdate.bind(this);
    this.handleEnterPress = this.handleEnterPress.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.sparklineDataIten = this.sparklineDataIten.bind(this);

    this.sparklineDataIten();
  }

  componentDidMount() {
    // $('.widget-container').sortable(sortOptions);
    this.initD3Chart();
  }

  sparklineDataIten() {
    for (let i = 0; i < this.tableSparklineValues.length; i += 1) {
      this.tableSparklineValues[i] = [
        10 + this.randomValue(), 15 + this.randomValue(),
        20 + this.randomValue(), 15 + this.randomValue(),
        25 + this.randomValue(), 25 + this.randomValue(),
        30 + this.randomValue(), 30 + this.randomValue(),
        40 + this.randomValue(),
      ];
    }
  }

  randomValue() {                           //eslint-disable-line
    return Math.floor(Math.random() * 40);
  }

  toggleFive() {
    this.setState({
      dropdownQuickAction: !this.state.dropdownQuickAction,
    });
  }

  toggleActiveTab(tabIndex) {
    this.setState({ activeTab: tabIndex });
  }

  initD3Chart() {
    const streamLayers = (n, m, o) => {
      if (arguments.length < 3) {
        o = 0; //eslint-disable-line
      }

      const bump = (a) => {
        const x = 1 / (0.1 + Math.random());
        const y = (2 * Math.random()) - 0.5;
        const z = 10 / (0.1 + Math.random());
        for (let i = 0; i < m; i += 1) {
          const w = ((i / m) - y) * z;
          a[i] += x * Math.exp(-w * w); //eslint-disable-line
        }
      };

      return d3.range(n).map(() => {
        const a = [];
        let i;
        for (i = 0; i < m; i += 1) {
          a[i] = o + (o * Math.random());
        }
        for (i = 0; i < 5; i += 1) {
          bump(a);
        }
        return a.map((d, iItem) => ({ x: iItem, y: Math.max(0, d) }));
      });
    };

    const testData = (streamNames, pointCount) => {
      const now = new Date().getTime();
      const day = 1000 * 60 * 60 * 24; // milliseconds
      const daysAgoCount = 60;
      const daysAgo = daysAgoCount * day;
      const daysAgoDate = now - daysAgo;
      const pointsCount = pointCount || 45; // less for better performance
      const daysPerPoint = daysAgoCount / pointsCount;
      return streamLayers(streamNames.length, pointsCount, 0.1).map((data, i) => ({
        key: streamNames[i],
        values: data.map(d => ({
          x: daysAgoDate + (d.x * day * daysPerPoint),
          y: Math.floor(d.y * 100), // just a coefficient,
        })),
        yAxis: i + 1,
        type: 'line',
      }));
    };

    nv.addGraph(() => {
      const chart = nv.models.lineChart()
        .useInteractiveGuideline(true)
        .margin({ left: 28, bottom: 30, right: 0 })
        .color(['#4380bf', '#38b05e'])
        .showLegend(true);
      chart.xAxis
        .showMaxMin(false)
        .tickFormat(d => d3.time.format('%b %d')(new Date(d)));
      chart.yAxis
        .showMaxMin(false)
        .tickFormat(d3.format(',f'));

      const chartData = testData(['Diesel', 'Gas'], 30);
      d3.select(this.nvd3ChartLineSvg)
        .style('height', '200px')
        .datum(chartData.map((el) => {
          el.area = true; //eslint-disable-line
          return el;
        }))
        .call(chart);

      d3.select(this.nvd3ChartLineSvgFuelCost)
        .style('height', '200px')
        .datum(chartData.map((el) => {
          el.area = true; //eslint-disable-line
          return el;
        }))
        .call(chart);

      d3.select(this.nvd3ChartLineSvgServiceCost)
        .style('height', '200px')
        .datum(chartData.map((el) => {
          el.area = true; //eslint-disable-line
          return el;
        }))
        .call(chart);

      d3.select(this.nvd3ChartLineSvgDriverRemittance)
        .style('height', '200px')
        .datum(chartData.map((el) => {
          el.area = true; //eslint-disable-line
          return el;
        }))
        .call(chart);

      return chart;
    });
  }


  messageInputUpdate(event) {
    this.setState({ newMessage: event.target.value });
  }

  handleEnterPress(event) {
    if (event.key === 'Enter') {
      this.addMessage();
    }
  }

  addMessage() {
    const message = {
      time: 'just now',
      sender: 'Tikhon Laninga',
      text: this.state.newMessage,
      image: i2,
    };

    this.setState({
      messages: [...this.state.messages, message],
      newMessage: '',
    });

    this.scrollbar.scrollToBottom();
  }

  render() {
    return (
      <div className={s.root}>
        <Row>
          <Col md={8}>
            <h2 className="page-title">Dashboard <small>Statistics and more</small></h2>
          </Col>
          <Col md={4}>
            &nbsp;
          </Col>
        </Row>
        <Row className="grid-demo">
          <Col className="widget-container" md={4}>
          <Widget
              title={<h5><i className="fa fa-lightbulb" />Active Work Order</h5>}
            >
              <ul className={s.overallStats}>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-folder-open-o" />&nbsp;
                  </div>
                  <span className={s.key}>Open</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-success">7 541</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-check-square-o" />&nbsp;
                  </div>
                  <span className={s.key}>Need Approval</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-danger">2 876</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-clock-o" />&nbsp;
                  </div>
                  <span className={s.key}>Waiting for parts</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-warning">68%</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="glyphicon glyphicon-shopping-cart" />&nbsp;
                  </div>
                  <span className={s.key}>In Shop</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-info">32%</span>
                  </div>
                </li>
              </ul>
              &nbsp;
              <Button color="inverse" size="sm" block>Show more ...</Button>
            </Widget>
            
          </Col>
          
          <Col md={4} className="widget-container">
          <Widget
              title={<h5>Service Reminders</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
              <Row>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Overdue
                  </div>
                </Col>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Due Soon
                  </div>
                </Col>
                <Button color="inverse" size="sm" block>Show more ...</Button>
              </Row>
            </Widget>
          </Col>
          <Col md={4} className="widget-container">
          <Widget
              title={<h5>Open Issues</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
              <Row>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Overdue
                  </div>
                </Col>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Due Soon
                  </div>
                </Col>
                <Button color="inverse" size="sm" block>Show more ...</Button>
              </Row>
            </Widget>
           
          </Col>
        </Row>
        <Row>
        <Col md={4} className="widget-container">
          <Widget
              title={<h5>Fuel Costs</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
            <svg
                ref={(r) => {
                  this.nvd3ChartLineSvgFuelCost = r;
                }}
              />
              <Button color="inverse" size="sm" block>Show more ...</Button>
            </Widget>
          </Col>
          <Col md={4} className="widget-container">
          <Widget
              title={<h5>Service Costs</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
            <svg
                ref={(r) => {
                  this.nvd3ChartLineSvgServiceCost = r;
                }}
              />
              <Button color="inverse" size="sm" block>Show more ...</Button>
            </Widget>
          </Col>
          <Col md={4} className="widget-container">
          <Widget
              title={<h5>Vehicle Renewal Reminders</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
            <Row>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Overdue
                  </div>
                </Col>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Due Soon
                  </div>
                </Col>
                <Button color="inverse" size="sm" block>Show more ...</Button>
              </Row>
            </Widget>
          </Col>
        </Row>
        <Row>
        <Col md={8} className="widget-container">
            <Widget
              title={
                <h4>Fleets <small>Based on a three months data</small></h4>
              } settings collapse close
            >
            <Row>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> Offline
                  </div>
                </Col>
                <Col md={6} className="box">
                  <div className="big-text">
                    19
                  </div>
                  <div className="description">
                    <i className="fa fa-user" /> On-line
                  </div>
                </Col>
              </Row>
              <ul className={s.overallStats}>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-folder-open-o" />&nbsp;
                  </div>
                  <span className={s.key}>Purchase Order</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-success">7 541</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-check-square-o" />&nbsp;
                  </div>
                  <span className={s.key}>Low</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-danger">2 876</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-clock-o" />&nbsp;
                  </div>
                  <span className={s.key}>Expire Soon</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-warning">68%</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="glyphicon glyphicon-shopping-cart" />&nbsp;
                  </div>
                  <span className={s.key}>Out of stock</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-info">32%</span>
                  </div>
                </li>
              </ul>
              &nbsp;
              <Button color="inverse" size="sm" block>Show more ...</Button>
            </Widget>
          </Col>
          <Col md={4} className="widget-container">
          <Widget
              title={<h5>Driver Remittance</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
            <svg
                ref={(r) => {
                  this.nvd3ChartLineSvgDriverRemittance = r;
                }}
              />
              <Button color="inverse" size="sm" block>Show more ...</Button>
            
            </Widget>
            <Widget
              title={<h5>Inventory</h5>}
              refresh collapse close
              showTooltip tooltipPlacement={tooltipPlacement}
              bodyClass={'`p-0`'}
            >
            <ul className={s.overallStats}>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-folder-open-o" />&nbsp;
                  </div>
                  <span className={s.key}>Purchase Order</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-success">7 541</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-check-square-o" />&nbsp;
                  </div>
                  <span className={s.key}>Low</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-danger">2 876</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="fa fa-clock-o" />&nbsp;
                  </div>
                  <span className={s.key}>Expire Soon</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-warning">68%</span>
                  </div>
                </li>
                <li>
                  <div className="icon float-left">
                    <i className="glyphicon glyphicon-shopping-cart" />&nbsp;
                  </div>
                  <span className={s.key}>Out of stock</span>
                  <div className="value float-right">
                    <span className="badge badge-pill badge-info">32%</span>
                  </div>
                </li>
              </ul>
              &nbsp;
              <Button color="inverse" size="sm" block>Show more ...</Button>
            </Widget>
           
          </Col>
        </Row>
        
      </div>
    );
  }
}

export default (withStyles(s)(Dashboard));
