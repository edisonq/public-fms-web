import { NEW_FLEETEXPENSE } from "../constants";
import configuration from "../config";


export const  createExpenseFleet = (postData) => dispatch => {
    fetch(configuration.api.serverUrl + "/expenses",{
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(postData)
    })
    .then(res => res.json())
    .then(data => 
        dispatch({
            type: NEW_FLEETEXPENSE,
            payload: data
        })
    );
};