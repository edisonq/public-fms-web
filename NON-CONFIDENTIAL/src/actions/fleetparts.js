import { FETCH_FLEETPARTS } from "../constants/fleetpart";
import configuration from "../config";


export const  fetchFleetParts = () => dispatch => {
	fetch(configuration.api.serverUrl + "/partsVehicle" )
		.then(res => res.json())
		.then(fleetsparts => 
			dispatch({
				type: FETCH_FLEETPARTS,
				payload: fleetsparts
			})
		);
};

// export const  createFetch = (postData) => dispatch => {
//     console.log('action called')
//     fetch(configuration.api.serverUrl + "/vehicles",{
//       method: 'POST',
//       headers: {
//         'Content-type': 'application/json'
//       },
//       body: JSON.stringify(postData)
//     })
//     .then(res => res.json())
//     .then(data => 
//         dispatch({
//             type: NEW_POST,
//             payload: data
//         })
//     );
// };