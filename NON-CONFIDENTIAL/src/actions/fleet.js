import { FETCH_FLEETS , NEW_FLEET } from "../constants/fleet";
import {FLEET_CHECKBOX} from '../constants/index';
import configuration from "../config";


export const  fetchFleets = () => dispatch => {
	fetch(configuration.api.serverUrl + "/vehicles" )
		.then(res => res.json())
		.then(fleets => 
			dispatch({
				type: FETCH_FLEETS,
				payload: fleets
			})
		);
};

export const  createFleet = (postData) => dispatch => {
    console.log('action called')
    fetch(configuration.api.serverUrl + "/vehicles",{
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(postData)
    })
    .then(res => res.json())
    .then(data => 
        dispatch({
            type: NEW_FLEET,
            payload: data
        })
    );
};

export const checkboxfleet = (checkboxData) => dispatch => {
    console.log('this action kay gitwag');
    // fetch(configuration.api.serverUrl + "/vehicles" )
	// 	.then(res => res.json())
	// 	.then(fleets => 
            dispatch({
                type: FLEET_CHECKBOX,
                payload: checkboxData
            });
    //     );

}