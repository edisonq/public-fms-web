import { FETCH_PARTS } from "../constants/part";
import configuration from "../config";

export const  fetchParts = () => dispatch => {
	fetch(configuration.api.serverUrl + "/parts" )
		.then(res => res.json())
		.then(parts => 
			dispatch({
				type: FETCH_PARTS,
				payload: parts
			})
		);
};
