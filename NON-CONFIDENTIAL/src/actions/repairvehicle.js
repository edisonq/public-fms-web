import { REPAIRVEHICLE_FETCH, REPAIR_CHECKBOX } from "../constants";
import configuration from "../config";

export const  fetchrepairparts = () => dispatch => {
	fetch(configuration.api.serverUrl + "/repairVehicle" )
		.then(res => res.json())
		.then(parts => 
			dispatch({
				type: REPAIRVEHICLE_FETCH,
				payload: parts
			})
		);
};

export const checkboxrepair = (checkboxData) => dispatch => {
	dispatch({
		type: REPAIR_CHECKBOX,
		payload: checkboxData
	});
};