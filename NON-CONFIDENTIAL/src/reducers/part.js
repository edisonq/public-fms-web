import { FETCH_PARTS } from '../constants/part';

const initialState = {
    parts: [],
    part: {}
}

export default function (state = initialState, action){
    switch (action.type) {
        case FETCH_PARTS:
            console.log('PART reducer activate');
            return {
                ...state,
                items: action.payload
            }
        default: 
            return state;
    }
}