import { FETCH_FLEETS } from '../constants/fleet';

const initialState = {
    fleets: [],
    fleet: {}
}

export default function (state = initialState, action){
    switch (action.type) {
        case FETCH_FLEETS:
            console.log('reducer FLEET activated');
            return {
                ...state,
                items: action.payload
            }
        // case NEW_FLEET:
        //     return {
        //         ...state,
        //         item: action.payload
        //     }
        default: 
            return state;
    }
}