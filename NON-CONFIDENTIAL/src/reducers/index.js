import { combineReducers } from 'redux';
import auth from './auth';
import navigation from './navigation';
import alerts from './alerts';
import mails from './mails';
import fleets from './fleet';
import repairvehicle from './repairvehicle';
import checkboxlist from './fleetcheckbox';
import fleetparts from './fleetparts'
import parts from './part';

export default combineReducers({
  alerts,
  auth,
  checkboxlist,
  fleetparts,
  fleets: fleets,
  navigation,
  mails,
  parts,
  repairvehicle  
});
