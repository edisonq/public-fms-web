import { REPAIRVEHICLE_FETCH } from '../constants';

const initialState = {
    repairvehicle: [],
}

export default function (state = initialState, action){
    switch (action.type) {
        case REPAIRVEHICLE_FETCH:
            console.log('nisulod sa repair PART reducer');
            return {
                ...state,
                repairvehicle: action.payload
            }
        // case NEW_REPAIRVEHICLE:
        //     return {
        //         ...state,
        //         item: action.payload
        //     }
        default: 
            return state;
    }
}